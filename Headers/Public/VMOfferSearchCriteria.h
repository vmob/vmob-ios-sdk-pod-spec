//
//  VMOfferSearchCriteria.h
//  VMobSDK
//
//  Created by VMob on 20/05/15.
//  Copyright (c) 2015 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMOffer.h"


@interface VMOfferSearchCriteria : NSObject

/*
 * Whether offer is ranked.
 */
@property (nonatomic, assign) BOOL rankedSearch;
/*
 * Keyword about offer.
 */
@property (nonatomic, copy) NSString* keyword;
/*
 * ID of a category, used to filter and get only offers from that category. CategoryId is an optional filter that can be supplied, pass an empty or nil value to search all categories.
 */
@property (nonatomic, copy) NSString* categoryId;
/*
 * ID of merchant.
 */
@property (nonatomic, copy) NSString* merchantId;
/*
 * Specify exact field names as an array of {@link #OfferFieldStruct} constant values to restrict the population of fields with data.
 */
@property (nonatomic, strong) NSArray* fields;
/*
 * OrderBy is case-sensitive and defaults to {@link #kVMOrderByWeighting}, other options are {@link #kVMOrderByTitle}, {@link #kVMOrderByLastUpdatedAt}  and {@link #kVMOrderByStartDate}.
 */
@property (nonatomic, copy) NSString* orderBy;
/*
 * Order direction options are `YES` for Ascending and `NO` for Descending.
 */
@property (nonatomic, assign) BOOL ascending;
/*
 * If 'YES', daypart offers are always returned even if they are not active at the moment of retrieval.
 */
@property (nonatomic, strong) NSNumber* ignoreDailyTimeFilter;
/*
 * If 'YES', offers are always returned for the week even if they are not active at a given day.
 */
@property (nonatomic, strong) NSNumber* ignoreDayFilter;
/*
 * Number of offers that should be skipped from the beginning of the search results.
 */
@property (nonatomic, strong) NSNumber* offset;
/*
 * Number of offers that should be returned.
 */
@property (nonatomic, strong) NSNumber* limit;
/*
 * The expression that will be used to filter the results using tags. E.g. "hot_deals AND expiring_soon OR new_deals".
 */
@property (nonatomic, copy) NSString* tagsFilterExpression;

@end

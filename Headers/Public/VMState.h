//
//  VMState.h
//  VMobSDK
//
//  Created by VMob on 2/12/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMBaseModel.h"
/**
 * VMob domain model - encapsulates State (in the context of geographical region) specific data. This inherits from {@link #VMBaseModel}.
 */
@interface VMState : VMBaseModel

/**
 * The state id.
 */
@property (nonatomic, strong) NSNumber *stateId;
/**
 * The state name.
 */
@property (nonatomic, strong) NSString *name;

@end

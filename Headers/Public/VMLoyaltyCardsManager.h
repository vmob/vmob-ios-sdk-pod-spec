//
//  VMLoyaltyCardsManager.h
//  VMobSDK
//
//  Created by VMob on 13/11/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMRequestManager.h"

/**
 * VMLoyaltyCardsManager is a subclass of {@link #VMRequestManager} designed for managing loyalty cards.
 */
@interface VMLoyaltyCardsManager : VMRequestManager

/**
 * The shared loyalty cards manager object for the system.
 *
 * @return The systemwide loyalty cards manager.
 */
+ (instancetype) sharedInstance;

/**  
 * Activates a new instance for the loyaltyCard with the given id.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param loyaltyCardId - The ID of the loyalty card to request an instance for.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) activateLoyaltyCardInstanceWithLoyaltyCardId:(NSNumber *)loyaltyCardId
                                               completionBlock:(VMobCompletionBlock)completionBlock;

/**  
 * Add the requested points to the active card instance.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param loyaltyCardId - The ID of the loyalty card to add points to.
 * @param pointsRequested - The number of points requested.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as VMLoyaltyCardCreatePoints* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) createPointsWithLoyaltyCardId:(NSNumber *)loyaltyCardId
                                pointsRequested:(NSNumber *)pointsRequested
                                completionBlock:(VMobCompletionBlock)completionBlock;


/**  
 * Add the requested points to the active card instance with an option to auto activate rewards.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param loyaltyCardId - The ID of the loyaltycard to add points to.
 * @param pointsRequested - The number of points requested.
 * @param transactionId - ID of the transaction for any future verifications.
 * @param anAutoActivateRewardFlag - A boolean flag to indicate whether to auto activate rewards or not.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as VMLoyaltyCardCreatePoints* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) createPointsWithLoyaltyCardId:(NSNumber *)loyaltyCardId
                                pointsRequested:(NSNumber *)pointsRequested
                                   transctionId:(NSString *)transactionId
                             autoActivateReward:(BOOL)anAutoActivateRewardFlag
                                completionBlock:(VMobCompletionBlock)completionBlock;


/**
 * Add the requested points to the active card instance with options to auto activate rewards and to fill multiple cards.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param loyaltyCardId - The id of the loyaltycard to add points to.
 * @param pointsRequested - The number of points requested.
 * @param transactionId - ID of the transaction for any future verifications.
 * @param anAutoActivateRewardFlag - A boolean flag to indicate whether to auto activate rewards or not.
 * @param aFillMultipleCardsFlag - A boolean flag to indicate whether to fill multiple cards or not.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as VMLoyaltyCardCreatePoints* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) createPointsWithLoyaltyCardId:(NSNumber *)loyaltyCardId
                                pointsRequested:(NSNumber *)pointsRequested
                                   transctionId:(NSString *)transactionId
                             autoActivateReward:(BOOL)anAutoActivateRewardFlag
                              fillMultipleCards:(BOOL)aFillMultipleCardsFlag
                                completionBlock:(VMobCompletionBlock)completionBlock;


/** 
 * Activates an offer for Redemption on an offer for a full card instance.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param loyaltyCardInstanceId - The ID of the loyalty card instance to redeem the offer with.
 * @param offerId - The ID of the offer to redeem.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) activateOfferWithLoyaltyCardInstanceId:(NSString *)loyaltyCardInstanceId
                                                 offerId:(NSNumber *)offerId
                                         completionBlock:(VMobCompletionBlock)completionBlock;

/**
 * Retrieves a list with all the pointRequests made by the Consumer.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param loyaltyCardId - The ID of the loyalty card (optional).
 * @param startDate - The start date of the list (optional).
 * @param endDate - The end date of the list (optional).
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMLoyaltyCardTransaction*> and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) listPointsTransactionsWithLoyaltyCardId:(NSNumber *)loyaltyCardId
                                                startDate:(NSDate *)startDate
                                                  endDate:(NSDate *)endDate
                                          completionBlock:(VMobCompletionBlock)completionBlock;
    
@end

/*
 * Arello Mobile
 * Mobile Framework
 * Except where otherwise noted, this work is licensed under a Creative Commons Attribution 3.0 Unported License
 * http://creativecommons.org/licenses/by/3.0
 */

#import <Foundation/Foundation.h>

/**
 * NSDictionary category - Convenience methods for validation.
 */

@interface NSDictionary(VMValidation)
/**
 * @param key - dictionary key.
 * @return non-NSNull object or nil.
 */
- (id) validObjectForKey: (id) key;
/**
 * @param key - dictionary key.
 * @param aClass - expected Class type.
 * @return Only aClass type object or nil.
 */
- (id) validObjectForKey: (id) key ofClass: (Class) aClass;
/**
 * @param key - dictionary key.
 * @return BOOL value which supports true for 'True'.
 */
- (BOOL) boolForKey: (id) key;

@end

/**
 * NSMutableDictionary category - Convenience methods for setting values non empty values.
 */
@interface NSMutableDictionary(Validation)
/**
 * Set object or NSNull if obj is nil.
 *
 * @param obj - dictionary value.
 * @param key - dictionary key.
 */
- (void) setObjectOrNSNull:(id)obj forKey: (id) key;
/**
 * Set object or does not nothing if obj is nil.
 *
 * @param obj - dictionary value.
 * @param key - dictionary key.
 */
- (void) setObjectOrDoNothing: (id) obj forKey: (id) key;

@end

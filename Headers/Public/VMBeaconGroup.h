//
//  VMBeaconGroup.h
//  VMobSDK
//
//  Created by VMob on 5/08/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMBaseModel.h"

/**
 * VMob domain model - encapsulates iBeacon group specific data. This inherits from {@link #VMBaseModel}.
 */
@interface VMBeaconGroup : VMBaseModel

/**
 * BeaconGroups' id.
 */
@property (nonatomic, strong) NSNumber *beaconGroupId;

/**
 * BeaconsGroups' name.
 */
@property (nonatomic, strong) NSString *name;


/**
 * BeaconsGroups' beacons.
 */
@property (nonatomic, strong) NSArray *beacons;


@end

//
//  VMLoyaltyCard.h
//  VMobSDK
//
//  Created by VMob on 18/03/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMContentModel.h"
#import "VMLoyaltyCardInstance.h"

/**
 * VMob domain model - encapsulates Loyalty Card specific data. This inherits from {@link #VMContentModel}.
 */
@interface VMLoyaltyCard : VMContentModel

/**
 * Card's max number of instances.
 */
@property (nonatomic, strong) NSNumber *maxInstances;

/**
 * Card's subtitle.
 */
@property (nonatomic, strong) NSString *subTitle;

/**
 * Card's instructions.
 */
@property (nonatomic, strong) NSString *instructions;

/**
 * Card's termsAndConditions.
 */
@property (nonatomic, strong) NSString *termsAndConditions;

/**
 * Card's required number of points.
 */
@property (nonatomic, strong) NSNumber *pointsRequired;

/**
 * Inital number of points to be assigned to a Card.
 */
@property (nonatomic, strong) NSNumber *initialPoints;

/**
 * Card's maximum points per day.
 */
@property (nonatomic, strong) NSNumber *maxPointsPerDay;

/**
 * Card's maximum point requests per day.
 */
@property (nonatomic, strong) NSNumber *maxPointsRequestsPerDay;

/**
 * Card's number of instances available.
 */
@property (nonatomic, strong) NSNumber *instancesAvailable;

/**
 * Card's instances of type VMLoyaltyCardInstance.
 */
@property (nonatomic, strong) NSArray *instances;

/**
 * Card's offers of type NSDictionary.
 */
@property (nonatomic, strong) NSArray *offers;

/**
 * Card's Category Id.
 */
@property (nonatomic, strong) NSNumber *categoryId;

/**
 * Card's Category Name.
 */
@property (nonatomic, strong) NSString *categoryName;

/**
 * Card's assetPath.
 */
@property (nonatomic, strong) NSString * assetPath;

@end

//
//  VMAdvertisement.h
//  VMobSDK
//
//  Created by VMob on 13/05/14.
//  Copyright (c) 2014 VMob All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMContentModel.h"
#import "VMVenue.h"

/**
 * VMob domain model - encapsulates advertisement specific data. This inherits from {@link #VMContentModel}, the content super class.
 */

@interface VMAdvertisement : VMContentModel
/**
 * Advertisement channelCode.
 */
@property(nonatomic, strong) NSString* channelCode;
/**
 * Advertisement clickThroughUrl.
 */
@property(nonatomic, strong) NSString* clickThroughUrl;
/**
 * Advertisement dateCreated.
 */
@property(nonatomic, strong) NSDate* dateCreated;
/**
 * Advertisement dateModified.
 */
@property(nonatomic, strong) NSDate* dateModified;
/**
 * Advertisement placementCode.
 */
@property(nonatomic, strong) NSString* placementCode;
/**
 * Advertisement venue list.
 */
@property(nonatomic, strong) NSArray* venues;
/**
 * Advertisement distanceToClosestVenue.
 */
@property(nonatomic, strong) NSNumber* distanceToClosestVenue;
/**
 * Advertisement closestVenue.
 */
@property(nonatomic, strong) VMVenue* closestVenue;
/**
 * Advertisement contentUrl.
 */
@property(nonatomic, strong) NSString* contentUrl;
/**
 * Advertisement contentTagReferenceCode list.
 */
@property(nonatomic, strong) NSArray* contentTagReferenceCodes;
/**
 * Advertisement daysOfWeek.
 */
@property(nonatomic, strong) NSArray* daysOfWeek;
/**
 * Advertisement isActive.
 */
@property(nonatomic, strong) NSNumber *isActive;
/**
 * Advertisement Start date.
 */
@property(nonatomic, strong) NSDate* eventStartDate;


/** 
 * @return the full url with the right CDN prefix.
 */
- (NSString *)absoluteImageUrlString;

/**
 * @param aWidth - image width.
 * @param aHeight - image height.
 *
 * @return the full url with the right CDN prefix with specified width and height.
 */
- (NSString *)absoluteImageUrlStringWithWidth:(int)aWidth andHeight:(int)aHeight;

/**
 * @return the full url with the right CDN prefix in JEPG format.
 */
- (NSString *)absoluteJpegImageUrlString;

/**
 * @param aWidth - image width.
 * @param aHeight - image height.
 *
 * @return the full url with the right CDN prefix with specified width and height in in JEPG format.
 */
- (NSString *)absoluteJpegImageUrlStringWithWidth:(int)aWidth andHeight:(int)aHeight;

@end

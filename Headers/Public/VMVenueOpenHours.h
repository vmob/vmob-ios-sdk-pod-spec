//
//  VMVenueOpenHours.h
//  VMobSDK
//
//  Created by VMob on 22/05/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseModel.h"
/**
 * VMob domain model - represents open hours for a Venue. This inherits from {@link #VMBaseModel}.
 */
@interface VMVenueOpenHours : VMBaseModel
/**
 * Opening day.
 */
@property(nonatomic, strong) NSString* day;
/**
 * Opening start time.
 */
@property(nonatomic, strong) NSString* start;
/**
 * Opening end time.
 */
@property(nonatomic, strong) NSString* end;
/**
 * Notes e.g. for a special day.
 */
@property(nonatomic, strong) NSString* note;

@end

//
//  VMBeacon+CLBeaconRegion.h
//  VMobSDK
//
//  Created by VMob on 6/08/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMBeacon.h"

#import <CoreLocation/CoreLocation.h>

/**
 * VMBeacon category class to help create {@link #VMBeacon} object from {@link #CLBeaconRegion}.
 */
@interface VMBeacon (CLBeaconRegion)
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
/**
 * Construct new instance of VMBeacon.
 *
 * @param aCLB - {@link #CLBeaconRegion} object.
 * @return new instance of {@link #VMBeacon}.
 */
- (instancetype)initWithCLBeaconRegion:(CLBeaconRegion *)aCLB;
#endif
@end

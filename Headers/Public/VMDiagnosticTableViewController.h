//
//  VMDiagnosticTableViewController.h
//  VMobSDK
//
//  Created by VMob on 23/10/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DDLogMessage;

/**
 * The protocol is used for capturing logging message by VMDiagnosticTableViewController.
 */
@protocol VMDiagnosticLoggerProtocol <NSObject>

/**
 * This method is called whenever logging some information
 *
 * @param message - message object with logging message, level and timestamp.
 */
- (void) addLog: (DDLogMessage *) message;

@end

/**
 * This prebuilt view controller helps with troubleshooting various VMob platform interactions and related configurations.
 * 
 * All push notifications will be displayed as well.
 */
@interface VMDiagnosticTableViewController : UITableViewController<VMDiagnosticLoggerProtocol>

@end

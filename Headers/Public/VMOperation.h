//
//  VMRequest.h
//  VMobSDK
//
//  Created by VMob on 4/05/15.
//  Copyright (c) 2015 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMRequestManager.h"

/**
 * VMOperation is used for cancelling request operation.
 */
@interface VMOperation : NSObject

/**
 * The weak reference to request manager.
 */
@property (nonatomic, weak) VMRequestManager *manager;

/**
 * Indicates whether operation is cancelled or not.
 */
@property (nonatomic, assign, readonly) BOOL isCancelled;

/**
 * Cancel the request operation.
 */
- (void)cancel;

@end

//
//  NSString+VMMultiTenancySupport.h
//  VMobSDK
//
//  Created by VMob on 30/09/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (VMMultiTenancySupport)

/**
 * This method appends &appid=appkey.
 *
 * @return self with &appid=appkey.
 */
- (NSString *)prepAssetForMultiTenancy;

@end

//
//  VMBaseModel.h
//  VMobSDK
//
//  Created by VMob on 3/06/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Base super abstract data model represents JSON data coming from VMob platform.
 */

@interface VMBaseModel : NSObject<NSCoding, NSCopying>

/**
 * @return contains the dictionary object returned from the server which was used to contruct the concrete object.
*/
@property(nonatomic, strong) NSDictionary* responseObject;

/**
 * Initialize new model object with dictionary representation.
 *
 * @param dicitionary - orginal JSON dictionary data.
 * @return A new model object.
*/
- (instancetype) initWithDictionary:(NSDictionary *) dictionary;

/**
 * Convert array of dictionary data in orginal dictionary into array of data models.
 *
 * @param dictionary - original JSON dictionary data.
 * @param key - one of element key in dictionary.
 * @param classType - data model class.
 * @return A array of classType data models.
 */
- (NSArray *) parsedArrayFromDictionary: (NSDictionary *) dictionary
                                 forKey: (NSString *) key
                         intoModelClass: (Class) classType;
@end

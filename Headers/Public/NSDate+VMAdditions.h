/*
 * Arello Mobile
 * Mobile Framework
 * Except where otherwise noted, this work is licensed under a Creative Commons Attribution 3.0 Unported License
 * http://creativecommons.org/licenses/by/3.0
 */

#import <Foundation/Foundation.h>

/**
 * NSDate category - Convenience methods for formatting and extracting date components.
 */
@interface NSDate(VMFormatExtensions)
/**
 * @param string - data in String format.
 * @param format - specified date format.
 * @return Parsed date object.
 */
+(NSDate*) dateFromString:(NSString*)string withFormat:(NSString*)format;
/**
 * @param format - specified date format.
 * @return String date.
 */
-(NSString*) stringValueWithFormat:(NSString*)format;
/**
 * @param style - format style.
 * @return String date with speficied NSDateFormatterStyle.
 */
-(NSString*) stringValueWithStyle:(NSDateFormatterStyle)style;
/**
 * @return week day.
 */
-(NSInteger) weekDay;
/**
 * @return date without time component.
 */
-(NSDate*) dateWithoutTimeComponent;
/**
 * @return date without time component.
 */
+(NSDate*) dateWithoutTimeComponent;
/**
 * @return date end time.
 */
-(NSDate*) dayEndTime;
/**
 * @param year - year.
 * @return year begin date for year.
 */
+(NSDate*) yearBeginDateForYear:(NSInteger) year;
/**
 * @param year - year.
 * @return year end date for year.
 */
+(NSDate*) yearEndDateForYear:(NSInteger) year;
/**
 * @return year begin date.
 */
+(NSDate*) yearBeginDate;
/**
 * @return year end date.
 */
+(NSDate*) yearEndDate;
/**
 * @param month - month.
 * @return month begin date for month.
 */
+(NSDate*) monthBeginDateForMonth:(NSInteger) month;
/**
 * @param month - month.
 * @return month end date for month.
 */
+(NSDate*) monthEndDateForMonth:(NSInteger) month;
/**
 * @return month begin date.
 */
+(NSDate*) monthBeginDate;
/**
 * @return month end date.
 */
+(NSDate*) monthEndDate;
/**
 * @return month begin date.
 */
-(NSDate*) monthBeginDate;
/**
 * @return month end date.
 */
-(NSDate*) monthEndDate;
/**
 * @param week - week.
 * @return week begin date for week.
 */
+(NSDate*) weekBeginDateForWeek:(NSInteger) week;
/**
 * @param week - week.
 * @return week end date for week.
 */
+(NSDate*) weekEndDateForWeek:(NSInteger) week;
/**
 * @return week begin date.
 */
+(NSDate*) weekBeginDate;
/**
 * @return week end date.
 */
+(NSDate*) weekEndDate;
/**
 * @return day.
 */
-(NSInteger) day;
/**
 * @return week.
 */
-(NSInteger) week;
/**
 * @return month.
 */
-(NSInteger) month;
/**
 * @return year.
 */
-(NSInteger) year;
/**
 * @return hour.
 */
-(NSInteger) hour;
/**
 * @return minute.
 */
-(NSInteger) minute;
/**
 * @return second.
 */
-(NSInteger) second;
/**
 * @param day - number of days.
 * @return date with addition of a number of days.
 */
-(NSDate*) dateByAddingDays:(NSInteger) day;
/**
 * @param months - number of months.
 * @return date with addition of a number of months.
 */
-(NSDate*) dateByAddingMonths:(NSInteger) months;
/**
 * @param years - number of years.
 * @return date with addition of a number of years.
 */
-(NSDate*) dateByAddingYears:(NSInteger) years;
/**
 * Parses dates from JSON responses.
 *
 * @param string - data in String format i.e. yyyy-MM-dd'T'HH:mm:ssZZZZZ.
 * @return Parsed date object.
 */
+ (NSDate *) parseJsonFormattedDate:(NSString *)string;

@end

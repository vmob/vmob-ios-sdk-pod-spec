//
//  VMAuthorizationManager.h
//  VMobSDK
//
//  Created by VMob on 13/11/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMRequestManager.h"
#import "VMConstant.h"

/**
 * VMAuthorizationManager is a subclass of {@link #VMRequestManager}, handles most of the basic registration/login process.
 * This class has methods to register with email id or password, login with email, link to external systems via cross references.
 * It also allows one to change the password and invoke a reset password workflow of a specific consumer.
 */

@interface VMAuthorizationManager : VMRequestManager {
@protected
    NSString * _apnsToken;
}

/**
 * The shared authorization manager object for the system.
 *
 * @return The systemwide authorization manager.
 */
+ (instancetype) sharedInstance;

/**
 * @return Indicates whether the consumer is authenticated or not.
 */
- (BOOL) isAuthenticated;

/**
 * @return APNS device token.
 */
- (NSString *) apnsToken;

/**
 * It is used in subsequent API calls internally.
 *
 * @return The latest Access Token received by the registration or login.
 */
- (NSString *) userToken;

#pragma mark - Register Methods

/**
 * The Create Mobile Phone Number Registration service is provided for registering of new Consumers using their mobile phone number as the Username. Calling the Create Mobile Phone Number Registration creates a new Consumer in the system, Logs the Consumer in and then creates and saves a valid OAuth Access Token. The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param mobilePhoneNumber - Consumer's phone number. It must contain an international prefix (+61 ....).
 * @param userName - Consumer's user name. It will be used to identify the consumer on the VMob system.
 * @param password - Consumer's password. Password minimal length is 6 characters.
 * @param firstName - Consumer's first name. Either First and Lastname must be present OR the full name.
 * @param lastName - Consumer's last name. Either First and Lastname must be present OR the full name.
 * @param fullName - Consumer's full name. Either First and Lastname must be present OR the full name.
 * @param gender - Consumer's gender.
 * @param dateOfBirth - Consumer's date of birth.
 * @param city - ID of the city where the consumer lives.
 * @param extendedData - NSString type extra data that doesn't fit as any of the other sign up params.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSDictionary* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) signUpWithMobilePhone: (NSString *) mobilePhoneNumber
                               userName: (NSString *) userName
                               password: (NSString *) password
                              firstName: (NSString *) firstName
                               lastName: (NSString *) lastName
                               fullName: (NSString *) fullName
                                 gender: (VMGender) gender
                            dateOfBirth: (NSDate *) dateOfBirth
                                   city: (NSString *) city
                           extendedData: (id) extendedData
                        completionBlock: (VMobCompletionBlock) completionBlock;


/**
 * The Create Mobile Phone Number Registration service is provided for registering of new Consumers using their mobile phone number as the Username and a verification token. Calling the Create Mobile Phone Number Registration creates a new Consumer in the system, Logs the Consumer in and then creates and saves a valid OAuth Access Token. The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param mobilePhoneNumber - Consumer's phone number. It must contain an international prefix (+61 ....).
 * @param userName - Consumer's user name. It will be used to identify the consumer on the VMob system.
 * @param password - Consumer's password. Password minimal length is 6 characters.
 * @param firstName - Consumer's first name. Either First and Lastname must be present OR the full name.
 * @param lastName - Consumer's last name. Either First and Lastname must be present OR the full name.
 * @param fullName - Consumer's full name. Either First and Lastname must be present OR the full name.
 * @param gender - Consumer's gender.
 * @param dateOfBirth - Consumer's date of birth.
 * @param city - ID of the city where the consumer lives.
 * @param extendedData - NSString type extra data that doesn't fit as any of the other sign up params.
 * @param verificationToken - token to be used for email based verification flow
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request NSDictionary* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) signUpWithMobilePhone: (NSString *) mobilePhoneNumber
                               userName: (NSString *) userName
                               password: (NSString *) password
                              firstName: (NSString *) firstName
                               lastName: (NSString *) lastName
                               fullName: (NSString *) fullName
                                 gender: (VMGender) gender
                            dateOfBirth: (NSDate *) dateOfBirth
                                   city: (NSString *) city
                           extendedData: (id) extendedData
                      verificationToken: (NSString *)verificationToken
                        completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * The Create Email Registration service is provided for registering of new Consumers using their email address as the Username. Calling the Create Email Registration creates a new Consumer in the system, Logs the Consumer in and then creates and saves a valid OAuth Access Token. The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param email - Consumer's email. The email address must be a valid email.
 * @param userName - Consumer's user name. It will be used to identify the consumer on the VMob system.
 * @param password - Consumer's password. Password minimal length is 6 characters.
 * @param firstName - Consumer's first name. Either First and Lastname must be present OR the full name.
 * @param lastName - Consumer's last name. Either First and Lastname must be present OR the full name.
 * @param fullName - Consumer's full name. Either First and Lastname must be present OR the full name.
 * @param gender - Consumer's gender.
 * @param dateOfBirth - Consumer's date of birth.
 * @param city - ID of the city where the consumer lives.
 * @param extendedData - NSString type extra data that doesn't fit as any of the other sign up params.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSDictionary* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) signUpWithEmail: (NSString *) email
                         userName: (NSString *) userName
                         password: (NSString *) password
                        firstName: (NSString *) firstName
                         lastName: (NSString *) lastName
                         fullName: (NSString *) fullName
                           gender: (VMGender) gender
                      dateOfBirth: (NSDate *) dateOfBirth
                             city: (NSString *) city
                     extendedData: (id) extendedData
                  completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * The Create Email Registration service is provided for registering of new Consumers using their email address as the Username and a verification token. Calling the Create Email Registration creates a new Consumer in the system, Logs the Consumer in and then creates and saves a valid OAuth Access Token. The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param email - Consumer's email. The email address must be a valid email.
 * @param userName - Consumer's user name. It will be used to identify the consumer on the VMob system.
 * @param password - Consumer's password. Password minimal length is 6 characters.
 * @param firstName - Consumer's first name. Either First and Lastname must be present OR the full name.
 * @param lastName - Consumer's last name. Either First and Lastname must be present OR the full name.
 * @param fullName - Consumer's full name. Either First and Lastname must be present OR the full name.
 * @param gender - Consumer's gender.
 * @param dateOfBirth - Consumer's date of birth.
 * @param city - ID of the city where the consumer lives.
 * @param extendedData - NSString type extra data that doesn't fit as any of the other sign up params.
 * @param verificationToken - token to be used for email based verification flow
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSDictionary* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) signUpWithEmail: (NSString *) email
                         userName: (NSString *) userName
                         password: (NSString *) password
                        firstName: (NSString *) firstName
                         lastName: (NSString *) lastName
                         fullName: (NSString *) fullName
                           gender: (VMGender) gender
                      dateOfBirth: (NSDate *) dateOfBirth
                             city: (NSString *) city
                     extendedData: (id) extendedData
                verificationToken: (NSString *) verificationToken
                  completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * The Create Mobile Phone Number Registration service is provided for registering of new Consumers using their mobile phone number as the Username. Calling the Create Mobile Phone Number Registration creates a new Consumer in the system, Logs the Consumer in and then creates and saves a valid OAuth Access Token. The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application. If the sign up is successful, a link to the external system ID received is created.
 *
 * This method is used when a consumer account needs to be linked to a 3rd party system user account (e.g. to a CRM system).
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param mobilePhoneNumber - Consumer's phone number. It must contain an international prefix (+61 ....)
 * @param userName - Consumer's user name. It will be used to identify the consumer on the VMob system.
 * @param password - Consumer's password. Password minimal length is 6 characters.
 * @param firstName - Consumer's first name. Either First and Lastname must be present OR the full name.
 * @param lastName - Consumer's last name. Either First and Lastname must be present OR the full name.
 * @param fullName - Consumer's full name. Either First and Lastname must be present OR the full name.
 * @param gender - Consumer's gender.
 * @param dateOfBirth - Consumer's date of birth.
 * @param city - ID of the city where the consumer lives.
 * @param extendedData - NSString type extra data that doesn't fit as any of the other sign up params.
 * @param externalId - The ID of the consumer on the external system.
 * @param crossReferenceType - The external system type to link the new consumer account to.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) signUpAndLinkToExternalSystemWithMobilePhone: (NSString *) mobilePhoneNumber
                                                      userName: (NSString *) userName
                                                      password: (NSString *) password
                                                     firstName: (NSString *) firstName
                                                      lastName: (NSString *) lastName
                                                      fullName: (NSString *) fullName
                                                        gender: (VMGender) gender
                                                   dateOfBirth: (NSDate *) dateOfBirth
                                                          city: (NSString *) city
                                                  extendedData: (id) extendedData
                                                    externalId: (NSString *) externalId
                                                    systemType: (VMCrossReferenceType) crossReferenceType
                                               completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * The Create Mobile Phone Number Registration service is provided for registering of new Consumers using their mobile phone number as the Username and a verification token. Calling the Create Mobile Phone Number Registration creates a new Consumer in the system, Logs the Consumer in and then creates and saves a valid OAuth Access Token. The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application. If the sign up is successful, a link to the external system ID received is created.
 *
 * This method is used when a consumer account needs to be linked to a 3rd party system user account (e.g. to a CRM system).
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param mobilePhoneNumber - Consumer's phone number. It must contain an international prefix (+61 ....)
 * @param userName - Consumer's user name. It will be used to identify the consumer on the VMob system.
 * @param password - Consumer's password. Password minimal length is 6 characters.
 * @param firstName - Consumer's first name. Either First and Lastname must be present OR the full name.
 * @param lastName - Consumer's last name. Either First and Lastname must be present OR the full name.
 * @param fullName - Consumer's full name. Either First and Lastname must be present OR the full name.
 * @param gender - Consumer's gender.
 * @param dateOfBirth - Consumer's date of birth.
 * @param city - ID of the city where the consumer lives.
 * @param extendedData - NSString type extra data that doesn't fit as any of the other sign up params.
 * @param externalId - The ID of the consumer on the external system.
 * @param crossReferenceType - The external system type to link the new consumer account to.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) signUpAndLinkToExternalSystemWithMobilePhone: (NSString *) mobilePhoneNumber
                                                      userName: (NSString *) userName
                                                      password: (NSString *) password
                                                     firstName: (NSString *) firstName
                                                      lastName: (NSString *) lastName
                                                      fullName: (NSString *) fullName
                                                        gender: (VMGender) gender
                                                   dateOfBirth: (NSDate *) dateOfBirth
                                                          city: (NSString *) city
                                                  extendedData: (id) extendedData
                                                    externalId: (NSString *) externalId
                                                    systemType: (VMCrossReferenceType) crossReferenceType
                                             verificationToken: (NSString *) verificationToken
                                               completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * The Create Email Registration service is provided for registering of new Consumers using their email address as the Username. Calling the Create Email Registration creates a new Consumer in the system, Logs the Consumer in and then creates and saves a valid OAuth Access Token.  The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application. If the sign up is successful, a link to the external system ID received is created.
 *
 * This method is used when a consumer account needs to be linked to a 3rd party system user account (e.g. to a CRM system).
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param email - Consumer's email. The email address must be a valid email.
 * @param userName - Consumer's user name. It will be used to identify the consumer on the VMob system.
 * @param password - Consumer's password. Password minimal length is 6 characters.
 * @param firstName - Consumer's first name. Either First and Lastname must be present OR the full name.
 * @param lastName - Consumer's last name. Either First and Lastname must be present OR the full name.
 * @param fullName - Consumer's full name. Either First and Lastname must be present OR the full name.
 * @param gender - Consumer's gender.
 * @param dateOfBirth - Consumer's date of birth.
 * @param city - ID of the city where the consumer lives.
 * @param extendedData - NSString type extra data that doesn't fit as any of the other sign up params.
 * @param externalId - The ID of the consumer on the external system.
 * @param crossReferenceType - The external system type to link the new consumer account to.
 * @param completionBlock The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) signUpAndLinkToExternalSystemWithEmail: (NSString *) email
                                                userName: (NSString *) userName
                                                password: (NSString *) password
                                               firstName: (NSString *) firstName
                                                lastName: (NSString *) lastName
                                                fullName: (NSString *) fullName
                                                  gender: (VMGender) gender
                                             dateOfBirth: (NSDate *) dateOfBirth
                                                    city: (NSString *) city
                                            extendedData: (id) extendedData
                                              externalId: (NSString *) externalId
                                              systemType: (VMCrossReferenceType) crossReferenceType
                                         completionBlock: (VMobCompletionBlock) completionBlock;
/**
 * The Create Email Registration service is provided for registering of new Consumers using their email address as the Username and a verification token. Calling the Create Email Registration creates a new Consumer in the system, Logs the Consumer in and then creates and saves a valid OAuth Access Token.  The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application. If the sign up is successful, a link to the external system ID received is created.
 *
 * This method is used when a consumer account needs to be linked to a 3rd party system user account (e.g. to a CRM system).
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param email - Consumer's email. The email address must be a valid email.
 * @param userName - Consumer's user name. It will be used to identify the consumer on the VMob system.
 * @param password - Consumer's password. Password minimal length is 6 characters.
 * @param firstName - Consumer's first name. Either First and Lastname must be present OR the full name.
 * @param lastName - Consumer's last name. Either First and Lastname must be present OR the full name.
 * @param fullName - Consumer's full name. Either First and Lastname must be present OR the full name.
 * @param gender - Consumer's gender.
 * @param dateOfBirth - Consumer's date of birth.
 * @param city - ID of the city where the consumer lives.
 * @param extendedData - NSString type extra data that doesn't fit as any of the other sign up params.
 * @param externalId - The ID of the consumer on the external system.
 * @param crossReferenceType - The external system type to link the new consumer account to.
 * @param completionBlock The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) signUpAndLinkToExternalSystemWithEmail: (NSString *) email
                                                userName: (NSString *) userName
                                                password: (NSString *) password
                                               firstName: (NSString *) firstName
                                                lastName: (NSString *) lastName
                                                fullName: (NSString *) fullName
                                                  gender: (VMGender) gender
                                             dateOfBirth: (NSDate *) dateOfBirth
                                                    city: (NSString *) city
                                            extendedData: (id) extendedData
                                              externalId: (NSString *) externalId
                                              systemType: (VMCrossReferenceType) crossReferenceType
                                       verificationToken: (NSString *) verificationToken
                                         completionBlock: (VMobCompletionBlock) completionBlock;

#pragma mark - Login Methods

/**
 * The Login with Username service is provided for existing Consumers to login using their username and password. The username will be either an email address or mobile phone number depending on how the Consumer had initially registered.  Login with Username will typically be used when logging into a second or subsequent Device, or via the browser etc. This is because in the normal process flow, a Consumer will register and the Access Token will be returned and cached. The Access Token is then must be used when starting the application instead of getting the Consumer to log in again.
 * Calling the Login with Username logs the Consumer into the system and creates and saves a valid OAuth Access Token.  The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param userName - Consumer's username, which can be his/her email address or phone number, depending on
 *                   how he/she had registered.
 * @param password - Consumer's password.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSDictionary* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) loginWithUserName: (NSString *) userName
                           password: (NSString *) password
                    completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * The Login with Username service is provided for existing Consumers to login using their username and password and a verification token. The username will be either an email address or mobile phone number depending on how the Consumer had initially registered.  Login with Username will typically be used when logging into a second or subsequent Device, or via the browser etc. This is because in the normal process flow, a Consumer will register and the Access Token will be returned and cached. The Access Token is then must be used when starting the application instead of getting the Consumer to log in again.
 * Calling the Login with Username logs the Consumer into the system and creates and saves a valid OAuth Access Token.  The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param userName - Consumer's username, which can be his/her email address or phone number, depending on
 *                   how he/she had registered.
 * @param password - Consumer's password.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSDictionary* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) loginWithUserName: (NSString *) userName
                           password: (NSString *) password
                  verificationToken: (NSString *) verificationToken
                    completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * The Login with Username service is provided for existing Consumers to login using their username and password. The username will be either an email address or mobile phone number depending on how the Consumer had initially registered.  Login with Username will typically be used when logging into a second or subsequent Device, or via the browser etc. This is because in the normal process flow, a Consumer will register and the Access Token will be returned and cached. The Access Token is then must be used when starting the application instead of getting the Consumer to log in again.
 * Calling the Login with Username logs the Consumer into the system and creates and saves a valid OAuth Access Token.  The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application.
 *
 * Additionally it is possible to request the server to return all the linked cross references and the current consumer information
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param userName - Consumer's username, which can be his/her email address or phone number, depending on
 *                   how he/she had registered.
 * @param password - Consumer's password.
 * @param returnCrossReferences - If true, all existing crossreferences for this consumer are returned.
 * @param returnConsumerInfo - If true, the current consumer information is returned.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSDictionary* and the error that occurred during the request.
 * @return A new cancellable operation.
*/

- (VMOperation *) loginWithUserName: (NSString *) userName
                           password: (NSString *) password
              returnCrossReferences: (BOOL) returnCrossReferences
                 returnConsumerInfo: (BOOL) returnConsumerInfo
                    completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * The Login with Username service is provided for existing Consumers to login using their username and password and verification token. The username will be either an email address or mobile phone number depending on how the Consumer had initially registered.  Login with Username will typically be used when logging into a second or subsequent Device, or via the browser etc. This is because in the normal process flow, a Consumer will register and the Access Token will be returned and cached. The Access Token is then must be used when starting the application instead of getting the Consumer to log in again.
 * Calling the Login with Username logs the Consumer into the system and creates and saves a valid OAuth Access Token.  The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application.
 *
 * Additionally it is possible to request the server to return all the linked cross references and the current consumer information
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param userName - Consumer's username, which can be his/her email address or phone number, depending on
 *                   how he/she had registered.
 * @param password - Consumer's password.
 * @param returnCrossReferences - If true, all existing crossreferences for this consumer are returned.
 * @param returnConsumerInfo - If true, the current consumer information is returned.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSDictionary* and the error that occurred during the request.
 * @return A new cancellable operation.
*/

- (VMOperation *) loginWithUserName: (NSString *) userName
                           password: (NSString *) password
              returnCrossReferences: (BOOL) returnCrossReferences
                 returnConsumerInfo: (BOOL) returnConsumerInfo
                  verificationToken: (NSString *) verificationToken
                    completionBlock: (VMobCompletionBlock) completionBlock;


/**
 * The Login with Social service is provided for either new or existing Consumers to login using 3rd party credentials. (e.g. Facebook / Twitter).  The API will resolve the 'Username' to a genuine 3rd party Access Token (e.g. in the case of Facebook it will just be a code which can then be used to retrieve an access token) and then retrieve basic user information, before returning a 'VMob' Access Token. The `socialType` defines which 3rd Party site and authentication method to use, e.g. {@link #VMFacebook}.
 * Calling the Login with Social logs the Consumer into the system and creates and saves a valid OAuth Access Token. The saved OAuth Access Token can then be used in for subsequent API calls to identify the Consumer of the Client Application.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param token - Social token.
 * @param defaultToConsumer - defaultToConsumer.
 * @param defaultToConsumerUsername - defaultToConsumerUsername.
 * @param defaultToConsumerPassword - defaultToConsumerPassword.
 * @param socialType - Defines which 3rd Party site and authentication method to use.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSDictionary* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) loginWithSocialToken: (NSString *) token
                     defaultToConsumer: (BOOL)defaultToConsumer
             defaultToConsumerUsername: (NSString *)defaultToConsumerUsername
             defaultToConsumerPassword: (NSString *)defaultToConsumerPassword
                            socialType: (VMSocialType) socialType
                       completionBlock: (VMobCompletionBlock) completionBlock;

#pragma mark - CrossReference Methods
/**
 * Cross References are created between the core data and 3rd parties such as GCM. For example, when a device is registered with GCM, it sends the registration through this api to create the cross reference between consumer and GCM.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param externalId - The ID of the consumer on the external system.
 * @param crossReferenceType - The external system type to link the new consumer account to.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) linkToExternalSystemWithExternalId: (NSString *) externalId
                                          systemType: (VMCrossReferenceType) crossReferenceType
                                     completionBlock: (VMobCompletionBlock) completionBlock;

#pragma mark - External
/**
 * Remove/Detach Social Connection.
 *
 * Detaches the current social account from the current user. This does not remove any other personal data.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param socialSource - the type of social connection.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/

- (VMOperation *) detachSocialAccountWithSocialSource:(VMSocialSource) socialSource
                                      completionBlock:(VMobCompletionBlock)completionBlock;

#pragma mark - Logout Methods
/**
 * The Logout service is provided for existing Consumers to logout. The consumer access token will be deleted and replaced by Device token.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSNumber* with boolean value and the error that occurred during the request.
 * @return A new cancellable operation.
*/

- (VMOperation *) logoutWithCompletionBlock: (VMobCompletionBlock) completionBlock;

#pragma mark - Password
/**
 * Requests to reset the password of the consumer which email address matches the received one.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param userName - Consumer's username, which can be his/her email address or phone number, depending on
 *                   how he/she had registered.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/


- (VMOperation *) requestPasswordResetWithUsername:(NSString *)username
                                   completionBlock:(VMobCompletionBlock)completionBlock;

/**
 * Change password, changes the current password to a new password.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param currentPassword - The current password of the consumer, used to verify that the person trying to change the
 *                          password is in fact the logged in consumer.
 * @param newPassword - The new password which will replace the old one.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */

- (VMOperation *) changePasswordWithCurrentPassword:(NSString *)currentPassword
                                    withNewPassword:(NSString *)newPassword
                                    completionBlock:(VMobCompletionBlock)completionBlock;

#pragma mark Social connection methods
/**
 * Creates or updates the social connection of the account.
 * If a social connection of the same type already exists, it will remove it and add this new one.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param socialSource - the type of social connection.
 * @param credential - the credentials of the connection. For example the facebook accessToken.
 * @param updateConsumerInfo - whether should update consumer information about social source.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */

- (VMOperation *) createUpdateSocialConnectionWithSocialSource: (VMSocialSource) socialSource
                                                    credential: (NSString *) credential
                                            updateConsumerInfo: (BOOL) updateConsumerInfo
                                               completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Change consumer account to anonymous.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param FBtoken - Facebook session token.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) changeConsumerFacebookAccountToAnonymousWithFBSessionToken:(NSString *)FBtoken
                                                             completionBlock:(VMobCompletionBlock)completionBlock;


/**
 * Disconnect consumer for social account.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param socialTypes - an array of social accounts, currently just "f" for Facebook is supported.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) disconnectSocialAccountWithSocialTypes:(NSArray *)socialTypes
                                         completionBlock:(VMobCompletionBlock)completionBlock;
@end

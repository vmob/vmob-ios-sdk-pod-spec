//
//  VMLocationsManager.h
//  VMobSDK
//
//  Created by VMob on 6/3/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "VMBaseLocationManager.h"

/**
 * VMLocationsManager is a subclass of {@link #VMBaseLocationManager} providing methods for retrieving the states and cities lists. This API should be used to select a location if gps/network location is not available on mobile device.
 */
@interface VMLocationsManager : VMBaseLocationManager

/**
 * Read-only variable showing if location is being updated: YES if the location is being updated, else NO.
 */
@property (nonatomic, readonly) BOOL isUpdatingLocation;

/**
 * Read-only variable showing the Time interval for updating location.
 */
@property (nonatomic, readonly) NSTimeInterval updateLocationTimeInterval;

/**
 * The shared locations manager object for the system.
 *
 * @return The systemwide locations manager.
 */
+ (instancetype) sharedInstance;

/**
 * Provides a list of all States.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMState*> and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) statesListWithCompletionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Provides a list of all Cities.
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMCity*> and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) citiesListWithCompletionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Provides a list of Cities for a given State.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param aStateId - The ID of state.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMCity*> and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) citiesListOfState:(NSString *)aStateId
                withCompletionBlock:(VMobCompletionBlock) completionBlock;

/**
 * Provides details of a given City.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param aCityId - the ID of city.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMCity*> and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) detailsOfCity:(NSString *)aCityId
            withCompletionBlock:(VMobCompletionBlock) completionBlock;

/**
 * Provides details of a given State.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param aStateId - The ID of state.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMState*> and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) detailsOfState:(NSString *)aStateId
             withCompletionBlock:(VMobCompletionBlock) completionBlock;


#pragma mark - Check Location

/**
 * Stop updating locations.
 */
- (void) stopLocation;

/**
 * Start updating locations.
 */
- (void) startLocation;

/**
 * Start updating locations.
 *
 * @param updateLocationBlock - block called on location update. Data parameter return as CLLocation*.
*/
- (void) startLocationWithCompletionBlock: (VMobCompletionBlock) updateLocationBlock;

/**
 * Set block called on successful location update.
 *
 * @param updateLocationBlock - block called on location update. Data parameter return as CLLocation*.
 */
- (void) setUpdateLocation: (VMobCompletionBlock) updateLocationBlock;

/**
 * Set block called on adding current location to activity batch.
 *
 * @param updateActivityLocationBlock - block called on adding current location to activity batch. Data parameter return as CLLocation*.
*/
- (void) setUpdateActivityLocation: (VMobCompletionBlock) updateActivityLocationBlock;

@end

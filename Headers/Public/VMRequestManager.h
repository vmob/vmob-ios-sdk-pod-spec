//
//  VMRequestManager.h
//  VMobSDK
//
//  Created by VMob on 5/29/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VMOperation;
/** 
  * The same block function in {@link #UIApplication} application:performFetchWithCompletionHandler:.
  */
typedef void (^BackgroundFetchResultBlock) (NSUInteger /*UIBackgroundFetchResult*/ result);

/**
 * The same block function in {@link #UIApplication} application:performFetchWithCompletionHandler:.
 */
typedef void (^BackgroundFetchResultBlock) (NSUInteger /*UIBackgroundFetchResult*/ result);

/**
 * Block function with two parameters passed back after service call.
 *
 * @param data - data returned from service call if successful
 * @param error - error object returned from service call if fails
 */
typedef void (^VMobCompletionBlock) (id data, NSError *error);

/**
 * VMob SDK error domain name.
 */
extern NSString *const kVMSDKErrorDomain;

/**
 * VMob SDK error code.
 */
typedef NS_ENUM(NSUInteger, VMSDKErrorCode) {
    VMSDKErrorCodeUnknown = 0,
    VMSDKErrorCodeAdvertisementApiUrlMissing = 601,
    VMSDKErrorCodeConsumerApiUrlMissing = 602,
    VMSDKErrorCodeOfferApiUrlMissing = 603,
    VMSDKErrorCodeActivityApiUrlMissing = 604,
    VMSDKErrorCodeLocationsApiUrlMissing = 605,
    VMSDKErrorCodeConfigurationApiUrlMissing = 606
};

/**
 * Notification name due to VMob platform down for maintenance.
 */
extern NSString *const VMobPlatformDownForMaintenance;

@class VMServiceProvider;
/**
 * VMRequestManager is a base class of API managers.
 */
@interface VMRequestManager : NSObject {
	@protected
	__strong VMServiceProvider *serviceProvider;
    __strong NSString* _apiName;
}

/**
 * @return request operation queue which executes request operations.
 */
- (NSOperationQueue *) operationQueue;

/*
 * @return specific error code as per the type of request manager.
 */
- (VMSDKErrorCode)errorCode;

/*
 * @return sepcific API URL used by request manager.
 */
- (NSString *)apiURL;

/*
 * Cancel a service request operation.
 *
 * @param operation - specified operation to be cancelled.
 */
- (void)cancelOperation: (VMOperation *) operation;

@end

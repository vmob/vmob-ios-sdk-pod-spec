//
//  NSDictionary+ServiceHelper.h
//  AMToolkit
//
//  Created by VMob on 25.02.13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDictionary+VMValidation.h"

/**
 * NSDictionary category - Convenience methods for parsing and conversion
 */
@interface NSDictionary (VMServiceHelper)

/**
 * Convert date string to date object.
 *
 * @param key - dictionary key refers to a value which has date string.
 * @param format - date format.
 * @return date object from String value in a dictionary.
 */
- (NSDate *) parseDateFromStringWithKey:(NSString *) key withFormat:(NSString *) format;

/**
 * Convert array of dictionary to array of specified object tpye.
 *
 * @param key - dictionary key refers to a value which has array of dictionary.
 * @param itemClass - specified class type.
 * @return array of specified object type.
 */
- (NSArray *) parseObjectsArrayFromArrayWithKey:(NSString *) key withClass:(Class) itemClass;

/**
 * Convert dictionary to String.
 *
 * @return String representation of JSON dictionary.
 */
- (NSString*) convertToJSONString;

@end

//
//  NSString+FavouriteContent.h
//  VMobSDK
//
//  Created by VMob on 2/06/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMConstant.h"

/**
 * NSString category - Convenience method for FavouriteContent type to string conversion.
 */

@interface NSString (FavouriteContent)

/**
 * Help method to convert enum {@link #VMFavourteContentType} value to String value.
 *
 * @param favouriteContentType - the enum value of favourite content type.
 * @return Favourite content type in String value.
 */
+ (NSString*) stringValueForContentType:(VMFavourteContentType) favouriteContentType;

@end

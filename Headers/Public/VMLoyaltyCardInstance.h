//
//  VMLoyaltyCardInstance.h
//  VMobSDK
//
//  Created by VMob on 18/03/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseModel.h"

/**
 * VMLoyaltyCardInstance is a class that encapsulates the LoyaltyCard instance information.
 */
@interface VMLoyaltyCardInstance : VMBaseModel

/**
 * Card's id.
 */
@property (nonatomic, strong) NSNumber *loyaltyCardId;

/**
 * Instance's id.
 */
@property (nonatomic, strong) NSString *loyaltyCardInstanceId;

/**
 * Indicator if instance is active.
 */
@property (nonatomic) BOOL active;

/**
 * The instance's points balance.
 */
@property (nonatomic, strong) NSNumber *pointsBalance;

/**
 * The instance's redeemed offer id.
 */
@property (nonatomic, strong) NSNumber *redeemedOfferId;

@end

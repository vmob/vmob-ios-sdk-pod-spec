//
//  VMLoyaltyCardTransaction.h
//  VMobSDK
//
//  Created by VMob on 19/03/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseModel.h"

/**
 * VMLoyaltyCardTransaction is a class that encapsulates a LoyaltyCard transaction.
 */

@interface VMLoyaltyCardTransaction : VMBaseModel

/**
 * The id of the loyalty card.
 */
@property (nonatomic, strong) NSNumber *loyaltyCardId;

/**
 * The id of the loyalty card instance.
 */
@property (nonatomic, strong) NSString *loyaltyCardInstanceId;

/**
 * The checkin date.
 */
@property (nonatomic, strong) NSDate *dateRequested;

/**
 * The number of points requested.
 */
@property (nonatomic, strong) NSNumber *pointsRequested;

/**
 * The number of points allocated.
 */
@property (nonatomic, strong) NSNumber *pointsAllocated;

/**
 * The transaction id.
 */
@property (nonatomic, strong) NSString *transactionId;

@end

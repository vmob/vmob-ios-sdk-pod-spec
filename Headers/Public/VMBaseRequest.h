//
//  VMBaseRequest.h
//  VMobSDK
//
//  Created by VMob on 5/29/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import "VMServiceRequest.h"

/**
 * Base abstract request class for gathering HTTP request headers and endpoint data.
 */

@interface VMBaseRequest : VMServiceRequest

/**
 * HTTP request headers.
 */
@property (nonatomic, readonly, strong) NSMutableDictionary *dictionaryHeader;

/**
 * HTTP request query parameters as part of URL/URI.
 */
@property (nonatomic, strong) NSMutableDictionary *dictionaryQueryParam;

/**
 * Construct the new instance of VMBaseRequest type object.
 *
 * @param token - access token.
 * @return a new instance.
 */
- (instancetype) initWithToken: (NSString *) token;
@end
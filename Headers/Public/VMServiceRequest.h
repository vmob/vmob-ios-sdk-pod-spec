/*
 * Arello Mobile
 * Mobile Framework
 * Except where otherwise noted, this work is licensed under a Creative Commons Attribution 3.0 Unported License
 * http://creativecommons.org/licenses/by/3.0
 */

#import <Foundation/Foundation.h>
#import "VMRequestError.h"
#import "VMHTTPStatusCodes.h"
#import "VMLog.h"

@class VMOperation;
/**
 * Abstract service request class for gathering HTTP response data and request data.
 */

@interface VMServiceRequest : NSObject

/**
 * HTTP response data.
 */
@property (nonatomic, strong) id responseData;

/**
 * HTTP request parameters.
 */
@property (nonatomic, strong) NSDictionary *requestDictionary;

/**
 * HTTP request operation e.g. it can be used for cancelling request.
 */
@property (nonatomic, strong, readonly) VMOperation *operation;

/**
 * @return HTTP content type e.g. application/json.
 */
- (NSString *) acceptableContentType;

/**
 * @return HTTP method e.g. POST.
 */
- (NSString *) method;

/**
 * @return HTTP URI endpoint.
 */
- (NSString *) path;

/**
 * Process orginal response data e.g. convert JSON to objects.
 *
 * @param response - original JSON data.
 */
- (void) processResponse: (id) response;

/**
 * Validate HTTP response by checking HTTP status code i.e. any status code other than 200, 201, 202, 203 is error.
 *
 * @param responseDictionary - orginal response data.
 * @param httpResponse - HTTP response object which contains HTTP stats code.
 * @return {@link #VMRequestError} if it is an error response.
 */
- (VMRequestError *) validateResponse:(NSDictionary*) responseDictionary httpResponse:(NSHTTPURLResponse *) httpResponse;

@end




//
//  VMHTTPStatusCodes.h
//
//  Created by VMob on 18.02.13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#ifndef AMToolkit_AMHTTPStatusCodes_h
#define AMToolkit_AMHTTPStatusCodes_h

/**
 * List of HTTP status used by VMob SDK.
 */
enum VMHTTPStatusCodes {
    /**
     * All OK.
     */
	HTTP_OK = 200,
    /**
     * Webpage created.
     */
	HTTP_CREATED = 201,
    /**
     * Accepted.
     */
	HTTP_ACCEPTED = 202,
    /**
     * No content.
     */
    HTTP_NO_CONTENT = 204,
    /**
     * Bad request sent to the server (e.g. some parameter was not added to the request).
     */
	HTTP_BAD_REQUEST = 400,
    /**
     * Unauthorised request sent to the server. Usually means there was no authorisation token added to
     * the request (e.g. the app has not registered the consumer, or the SDK has not registered the device).
     */
	HTTP_UNAUTHORIZED = 401,
    /**
     * A payment is required to perform this request. This code is unique to deployments where the
     * Payment Gateway is configured and enabled.
     */
    HTTP_PAYMENT_REQUIRED = 402,
    /**
     * The request sent is forbidden (e.g. it was sent from untrusted network).
     */
	HTTP_FORBIDDEN = 403,
    /**
     * Resource not found.
     */
	HTTP_NOT_FOUND = 404,
    /**
     * There was a conflict with the request sent (e.g. the email address provided has already been
     * registered and cannot be used again). For some APIs a code will be returned to identify the
     * exact error.
     */
    HTTP_CONFLICT = 409,
    /**
     * Used for redirects.
     */
	HTTP_MOVED_PERMANENTLY = 301,
    /**
     * HTTP found.
     */
	HTTP_FOUND = 302,
    /**
     * Service exception(may not have request body, or request body will contain html with error description).
     */
	HTTP_INTERNAL_SERVER_ERROR = 500,
    /**
     * Method not implemented.
     */
	HTTP_NOT_IMPLEMENTED = 501,
    /**
     * Gateway problems.
     */
	HTTP_BAD_GATEWAY = 502,
    /**
     * The server is under maintenance.
     */
	HTTP_SERVICE_UNAVAILABLE = 503,
    /**
     * Gateway timeout.
     */
	HTTP_GATEWAY_TIMEOUT = 504
};

#endif

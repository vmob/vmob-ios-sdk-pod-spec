//
//  VMContentModel.h
//  VMobSDK
//
//  Created by VMob on 17/11/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMBaseModel.h"

/**
 * Content data model represents content JSON data coming from VMob platform, such as offers, advertisements etc.
 */

@interface VMContentModel : VMBaseModel

@property (nonatomic, strong) id contentId;
@property (nonatomic, strong) NSNumber * weight;// Except for RedeemedOffer
@property (nonatomic, strong) NSString * extendedData;// Except for RedeemedOffer
@property (nonatomic, strong) NSNumber * merchantId;// Except for LoyaltyCard
@property (nonatomic, strong) NSDate * startDate;// Except for Venue
@property (nonatomic, strong) NSDate * endDate;// Except for Venue
@property (nonatomic, strong) NSString * title;// Except for Venue
@property (nonatomic, strong) NSString * contentImage;// Except for Venue
@property (nonatomic, strong) NSString * contentDescription;// Except for Venue

/*
 * daily start time in minutes from midnight
*/
@property(nonatomic, strong) NSNumber * dailyStartTime;

/*
 * daily end time in minutes from midnight
 */
@property(nonatomic, strong) NSNumber * dailyEndTime;

@end

//
//  VMCity.h
//  VMobSDK
//
//  Created by VMob on 2/12/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMBaseModel.h"
/**
 * VMob domain model - encapsulates city specific data. This inherits from {@link #VMBaseModel}.
 */
@interface VMCity : VMBaseModel

/**
 * The city id.
 */
@property (nonatomic, strong) NSNumber *cityId;
/**
 * The city name.
 */
@property (nonatomic, strong) NSString *name;
/**
 * The city GPS latitude.
 */
@property (nonatomic, strong) NSNumber* latitude;
/**
 * The city GPS longitude.
 */
@property (nonatomic, strong) NSNumber* longitude;
/**
 * The state to which this city belongs to.
 */
@property (nonatomic, strong) NSNumber *stateId;

@end

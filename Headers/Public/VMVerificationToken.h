//
//  VMVerificationToken.h
//  VMobSDK
//
//  Created by Vmob on 14/09/15.
//  Copyright © 2015 VMob. All rights reserved.
//

#import "VMBaseModel.h"

@interface VMVerificationToken : VMBaseModel

/**
 *  The verification token to show on the app, which can be used to identify the current
 *  consumer on a client's POS.
 */
@property (nonatomic, strong) NSString *verificationToken;

/**
 * The expiration date for this verification token. If this token is used after this date,
 * the backend will return an error.
 */
@property (nonatomic, strong) NSDate *expiryDate;


@end

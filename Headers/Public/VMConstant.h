//
//  VMConstant.h
//  VMobSDK
//
//  Created by VMob on 5/31/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#ifndef VMobSDK_VMConstant_h
#define VMobSDK_VMConstant_h

#define IsIOS7 [[UIDevice currentDevice].systemVersion floatValue] >= 7

typedef enum {
  VMFacebook = 0,
  VMFacebookAT = 2,
  VMTwitter = 1,
} VMSocialType;

typedef enum {
  VMMale = 0,
  VMFemale = 1,
  VMUnknown = 2
} VMGender;

typedef enum {
  VMCrossReferenceGCM = 0,
  VMCrossReferenceMSISDN = 2,
  VMCrossReferenceFacebookId = 3,
  VMCrossReferenceFacebookToken = 4,
  VMCrossReferenceCommuniq = 5,
  VMCrossReferenceApplePushNotification = 6,
  VMCrossReferenceBlackberry = 7,
  VMCrossReferenceWindowsNotificationService = 8,
  VMCrossReferenceCRM_1 = 9,
  VMCrossReferenceCMS_1 = 10,
  VMCrossReferenceFeliCa = 11
} VMCrossReferenceType;


/**
 * Lists the possible ways of sorting a list of offers supported by the system.
 */
static NSString * const kVMOrderByWeighting = @"Weighting";
static NSString * const kVMOrderByTitle = @"Title";
static NSString * const kVMOrderByLastUpdatedAt = @"LastUpdatedAt";
static NSString * const kVMOrderByStartDate = @"StartDate";


#pragma mark - Activity API

typedef enum {
  VMButtonClick,
  VMLocationCheckin,
  VMAppPageImpression,
  VMOfferImpression,
  VMOfferClickThrough,
  VMAppStartup,
  VMOfferShare,
  VMAdvertisementImpression,
  VMAdvertisementClick,
  VMAppInstall,
  VMLoyaltyImpression,
  VMBeaconCheckin,
  VMBeaconCheckout,
  VMPushClicked
} VMActionTypeCode;

typedef enum {
  VMGPS = 1,
  VMCell = 2,
  VMWifi =3,
  VMGEOEntry = 4,
  VMGEOExit = 5,
} VMTypeGPS;

typedef enum {
  VMAEmail = 1,
  VMAMobileNumber = 2,
  VMAFacebook = 3,
  VMATwitter = 4,
  VMAMSISDN = 5,
} VMActivitySocialType;

typedef enum {
  VMSEmail = 1,
  VMSFacebook = 2,
  VMSTwitter = 3,
} VMShareSocialType;


typedef enum {
  VMSocialSourceFacebookOAuth2,
  VMSocialSourceFacebookAccessToken,
} VMSocialSource;

typedef enum {
  VMFavouriteContentTypeOffer = 1,
  VMFavouriteContentTypeVenue = 2,
  VMFavouriteContentTypeAdvertisement = 3,
  VMFavouriteContentTypeMerchant = 4
} VMFavourteContentType;


typedef NS_ENUM(NSInteger, VMOfferRedemptionType)
{
  VMOfferRedemptionType_Unknown = -1,
  VMOfferRedemptionType_Image = 0,
  VMOfferRedemptionType_Text = 1,
  VMOfferRedemptionType_UniqueText = 2,
  VMOfferRedemptionType_PredefinedListText = 3,
  VMOfferRedemptionType_TemporaryText = 4
};



static NSString * const kWeightedContentIncudeTypeOffer = @"offer";
static NSString * const kWeightedContentIncudeTypeVenue = @"venue";
static NSString * const kWeightedContentIncudeTypeAdvertisement = @"advertisement";

static NSString * const VMobErrorDeviceRegistrationFailure = @"Device registration failed";

#pragma mark Notification
static NSString *const VMobBeaconEntryNotification = @"kVMBeaconEntryNotification";
static NSString *const VMobBeaconExitNotification = @"kVMBeaconExitNotification";

static NSString *const VMobGeoFenceEntryNotification = @"VMobGeoFenceEntryNotification";
static NSString *const VMobGeoFenceExitNotification = @"VMobGeoFenceExitNotification";
static NSString *const VMobGeoFenceUpdatedNotification = @"VMobGeoFenceUpdateNotification";
static NSString *const VMobGeoFenceLoggingNotification = @"VMobGeoFenceLoggingNotification";

static NSString *const VMobSDKDidFinishLoadingNotification = @"VMobSDKDidFinishLoadingNotification";

static NSString *const VMVerificationTokenNotification = @"VMVerificationTokenNotification";

static NSString *const VMBeaconNotificationUserInfoKey = @"VMBeacon";
static NSString *const VMGeoFencesNotificationUserInfoKey = @"VMGeoFences";

static NSString *const VMImageFormatJEPG = @"jpeg";
static NSString *const VMImageFormatPNG = @"png";

//VMobSDKConfigurarion plist keys that can be overriden by app
static NSString *const VMobSDKConfigurationKey_AutoStartSDK = @"autoStartVMobSDK";
static NSString *const VMobSDKConfigurationKey_AutoRefreshConfig = @"addCodeForAutoConfgurationUpdate";
static NSString *const VMobSDKConfigurationKey_AutoRefreshConfigInterval = @"configurationCheckInterval";
static NSString *const VMobSDKConfigurationKey_AutoStartLocationService = @"addCodeForAutoLocationServices";
static NSString *const VMobSDKConfigurationKey_AutoStartGeoFenceService = @"autostartGeoFenceService";
static NSString *const VMobSDKConfigurationKey_AutoStartBeaconService = @"autostartBeaconService";
static NSString *const VMobSDKConfigurationKey_ShowLocationDisabledAlert = @"enableLocationServiceAlert";
static NSString *const VMobSDKConfigurationKey_LocationRefreshTimeIntervalInSeconds = @"gpsPollIntervalInSeconds";
static NSString *const VMobSDKConfigurationKey_LocationAccuracy = @"locationAccuracy";
static NSString *const VMobSDKConfigurationKey_LocationDistanceFilter = @"locationDistanceFilter";
static NSString *const VMobSDKConfigurationKey_StartupActivityIntervalInMinutues = @"startupActivityInterval";

//VMobSDKConfigurarion Logging options
static NSString *const VMobSDKConfigurationKey_LogEnabled = @"logEnabled";
static NSString *const VMobSDKConfigurationKey_LogLevel = @"logLevel";
static NSString *const VMobSDKConfigurationKey_LogToFile = @"logToFile";





#endif
//
//  VMBeaconManager.h
//  VMobSDK
//
//  Created by VMob on 6/08/14.
//  Copyright (c) 2014 VMob Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VMBeacon.h"
#import "VMConstant.h"
#import "VMBaseLocationManager.h"

typedef void (^VMBeaconInteractionBlock) (VMBeacon *aBeacon, VMBeaconActivity activity, VMBeaconProximity proximity);
/**
 * VMob manager class - provides various methods to handle iBeacon monitoring activities.
 * Inherits from {@link #VMBaseLocationManager}
 */
@interface VMBeaconManager : VMBaseLocationManager

/**
 * The shared beacon manager object for the system.
 *
 * @return The systemwide beacon manager.
 */
+ (instancetype)sharedInstance;

/**
 * Use this method to monitor a list of beacon regions of the format "UUID,UUID".
 * NOTES: SDK automatically calls this method when app becomes active and VMob platform passes a list of beacons to be monitored.
 *
 * @param aRegionCSV - String list of beacon ids to be monitored, e.g. uuid, uuid, uuid.
 * @param aBeaconInteractionBlock - The block is executed when a beacon entry or exit is detected. This block has no return value and takes three arguments: detected beacon which enters or exits the region, beacon activity type and beacon proximity.
 */
- (void)startMonitoringBeaconRegions:(NSString *)aRegionCSV withInteractionBlock:(VMBeaconInteractionBlock)aBeaconInteractionBlock;

/**
 * To stop all beacon monitoring for the VMBeacon instances provided.
 */
- (void)stopMonitoringBeacons;

@end

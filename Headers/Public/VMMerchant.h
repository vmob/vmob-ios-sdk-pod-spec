//
//  VMMerchant.h
//  VMobSDK
//
//  Created by VMob on 2/06/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseModel.h"

/**
 * VMob domain model - encapsulates Merchant specific data. This inherits from {@link #VMBaseModel}.
 */
@interface VMMerchant : VMBaseModel
/**
 * Merchant id.
 */
@property(nonatomic, strong) NSNumber* merchantId;
/**
 * Merchant name.
 */
@property(nonatomic, strong) NSString* merchantName;
/**
 * Merchant organisation type.
 */
@property(nonatomic, strong) NSString* organistationType;

@end

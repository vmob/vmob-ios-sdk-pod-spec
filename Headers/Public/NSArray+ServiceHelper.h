//
//  NSArray+ServiceHelper.h
//  VMobSDK
//
//  Created by VMob on 19/03/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * NSArray category - Web Service convenience methods for parsing contained objects of a given Class type.
 */
@interface NSArray (ServiceHelper)

/**
 * Convert array of dictionary to array of specified object type.
 *
 * @param itemClass - specified class type.
 * @return array of specified object type.
 */
- (NSArray *) parseObjectsWithClass:(Class) itemClass;

@end

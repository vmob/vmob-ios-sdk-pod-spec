//
//  VMBeacon.h
//  VMobSDK
//
//  Created by VMob on 5/08/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMBaseModel.h"

/**
 * Beacons' proximity.
 */
typedef NS_OPTIONS(NSInteger, VMBeaconProximity)
{
    VMBeaconProximityUnknown = 0,
    VMBeaconProximityFar = 1 << 0,
    VMBeaconProximityNear = 1 << 1,
    VMBeaconProximityImmediate = 1 << 2
};

/**
 * Beacons' activity type.
 */
typedef NS_OPTIONS(NSInteger, VMBeaconActivity)
{
    VMBeaconActivityEntry = 0,
    VMBeaconActivityExit = 1 << 0,
    VMBeaconActivityEntryAndExit = 2 << 0
};

/**
 * VMob domain model - encapsulates iBeacon specific data. This inherits from {@link #VMBaseModel}.
 */
@interface VMBeacon : VMBaseModel

/**
 * Beacons' id.
 */
@property (nonatomic, strong) NSNumber *beaconId;

/**
 * Beacons' uuid.
 */
@property (nonatomic, strong) NSString *uuid;

/**
 * Beacons' major number.
 */
@property (nonatomic, strong) NSNumber *major;

/**
 * Beacons' minor number.
 */
@property (nonatomic, strong) NSNumber *minor;

/**
 * Beacons' signal strength.
 */
@property (nonatomic, strong) NSNumber *rssi;

/**
 * Beacon specific flags to range them or not.
 */
@property (nonatomic, readwrite) BOOL needsRanging;

/**
 * Beacon's proximity - last proximity ide.
 */
@property (nonatomic, readwrite) VMBeaconProximity proximity;

/**
 * Beacon's proximity filter  - used only when filtering beacon ranging.
 */
@property (nonatomic, readwrite) VMBeaconProximity proximityFilter;

/**
 * Beacon specific flags to track entry / exit or not.
 */
@property (nonatomic, readwrite) BOOL needsActivity;

/**
 * Beacon's last activity tracked - could be entry or exit.
 */
@property (nonatomic, readwrite) VMBeaconActivity activity;

/**
 * Beacon's activity filter - used only for entry and exit filtering.
 */
@property (nonatomic, readwrite) VMBeaconActivity activityFilter;


/**
 * Beacon specific flags to announce entry when device's display is on.
 */
@property (nonatomic, readwrite) BOOL notifyEntryStateOnDisplay;

/**
 * Beacon's composite key in the format uuid:major:minor.
 */
- (NSString *)compositeKey;

/**
 * Beacon entry time stamp
 */
@property (nonatomic, strong) NSDate *timeStamp;


/**
 * initializer to covert a CSV string of the format uuid:major:minior:timestamp to beacon object
 */
+ (instancetype)initWithCSVString:(NSString *)aCSVString;

/**
 * to convert beacon object to a CVS string of the format uuid:major:minior:timestamp
 */
- (NSString *)toCSVString;

/**
 * to convert beacon object to a Header CVS string of the format uuid:major:minior
 */
- (NSString *)toHeaderCSVString;
@end

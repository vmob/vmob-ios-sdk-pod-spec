//
//  VMExternalMerchant.h
//  VMobSDK
//
//  Created by VMob on 6/11/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMBaseModel.h"
/**
 * VMob domain model - encapsulates External Merchant specific data. This inherits from {@link #VMBaseModel}.
 */
@interface VMExternalMerchant : VMBaseModel
/**
 * External merchant id.
 */
@property(nonatomic, strong) NSNumber* merchantId;
/**
 * External merchant name.
 */
@property(nonatomic, strong) NSString* name;
/**
 * External merchant extra data.
 */
@property(nonatomic, strong) NSString* extendedData;
/**
 * External merchant description.
 */
@property(nonatomic, strong) NSString* desc;
/**
 * External merchant related external id.
 */
@property(nonatomic, strong) NSString* externalId;
/**
 * External merchant whether hidden or visible.
 */
@property(nonatomic, readwrite) BOOL isHidden;
/**
 * External merchant address1.
 */
@property(nonatomic, strong) NSString* addressLine1;
/**
 * External merchant address2.
 */
@property(nonatomic, strong) NSString* addressLine2;
/**
 * External merchant address3.
 */
@property(nonatomic, strong) NSString* addressLine3;
/**
 * External merchant email address.
 */
@property(nonatomic, strong) NSString* emailAddress;
/**
 * External merchant phone number.
 */
@property(nonatomic, strong) NSString* phoneNumber;
/**
 * External merchant post code.
 */
@property(nonatomic, strong) NSString* postCode;
/**
 * External merchant website URL.
 */
@property(nonatomic, strong) NSString* websiteUrl;

@end

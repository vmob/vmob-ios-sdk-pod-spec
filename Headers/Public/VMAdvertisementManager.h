//
//  VMAdvertisementManager.h
//  VMobSDK
//
//  Created by VMob on 10/04/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMRequestManager.h"

/**
 * VMAdvertisementManager is a subclass of {@link #VMRequestManager} provided to interact with VMob Advertisement.
 */

@interface VMAdvertisementManager : VMRequestManager

/**
 * The shared Advertisement manager object for the system.
 *
 * @return The systemwide Adveristement manager.
 */
+ (instancetype) sharedInstance;

/**
 * Gets a list of advertisements in the system for a given channel and placement type.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param channel - used to filter the advertisements (e.g. mobile app, etc.). This value is optional
 *                  and can be set to nil, however if used, it must match an exact string configured
 *                  on the server for a specific channel.
 * @param placement - Place where the advertisement will be positioned.
 * @param limit - Amount of advertisements that should be returned.
 * @param completionBlock The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMAdvertisement*> and the error that occurred during the request.
 * @return A new cancellable operation.

*/
- (VMOperation *)advertisementListWithChannel: (NSString *)  channel
                                    placement: (NSString *)  placement
                                        limit: (NSNumber *)  limit
                              completionBlock: (VMobCompletionBlock) completionBlock;


/**
 * Gets a list of advertisements in the system for a given channel and placement type.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param channel - used to filter the advertisements (e.g. mobile app, etc.). This value is optional
 *                  and can be set to nil, however if used, it must match an exact string configured
 *                  on the server for a specific channel.
 * @param placement - Place where the advertisement will be positioned.
 * @param limit - Amount of advertisements that should be returned.
 * @param ignoreDailyTimeFilter - If 'YES', daypart advertisements are always returned even if they are not active at the moment of retrieval.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMAdvertisement*> and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *)advertisementListWithChannel: (NSString *)  channel
                                    placement: (NSString *)  placement
                                        limit: (NSNumber *)  limit
                        ignoreDailyTimeFilter: (BOOL) ignoreDailyTimeFilter
                              completionBlock: (VMobCompletionBlock) completionBlock;



@end

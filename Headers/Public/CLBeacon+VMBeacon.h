//
//  CLBeacon+VMBeacon.h
//  VMobSDK
//
//  Created by VMob on 7/08/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "VMBeacon.h"

/**
 * CLBeacon category class to help get expected value type from existing properties.
 */

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000

@interface CLBeacon (VMBeacon)

/**
 * @return NSString of proximityUUID + major + minor.
 */
- (NSString *)compositeKey;

/**
 * @return proximity in NSString.
 */
- (NSString *)proximityLabel;

/**
 * @return proximity in VMBeaconProximity type.
 */
- (VMBeaconProximity)toVMBeaconProximity;

@end

#endif
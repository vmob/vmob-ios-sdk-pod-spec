//
//  VMGeoFence.h
//  VMobSDK
//
//  Created by VMob on 29/09/15.
//  Copyright (c) 2015 VMob. All rights reserved.
//

#import "VMBaseModel.h"
/**
 * VMob domain model - encapsulates geofence specific data. This inherits from {@link #VMBaseModel}.
 */
@interface VMGeoFence : VMBaseModel
/**
 * The geofence id.
 */
@property (nonatomic, strong) NSString *geofenceId;
/**
 * The city name.
 */
@property (nonatomic, strong) NSNumber *radius;
/**
 * The geofence GPS latitude.
 */
@property (nonatomic, strong) NSNumber* latitude;
/**
 * The geofence GPS longitude.
 */
@property (nonatomic, strong) NSNumber* longitude;

@end

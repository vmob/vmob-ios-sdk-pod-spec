//
//  VMWeightedContentListCriteria.h
//  VMobSDK
//
//  Created by VMob on 16/06/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 * This class can be used to represent a query criteria for searching weighted content of type Advertisement/Offer/Venue.
 */
@interface VMWeightedContentListCriteria : NSObject

/**

 @param regionId, optional
 @param includeRelatedOffers, optional, if true Offers the Venue Offers will be populated
 */


/* Overrides the latitude in the request header, optional */
@property(nonatomic, strong) NSNumber* latitude;

/* Overrides the longitude in the request header, optional */
@property(nonatomic, strong) NSNumber* longitude;

/* Optional */
@property(nonatomic, strong) NSNumber* categoryId;

/**
 * comma delimited string, optional.
 * An expression to filter the results using tags. E.g. "hot_deals AND expiring_soon OR new_deals".
 */
@property(nonatomic, strong) NSString* tagsFilterExpression;

/* accepts a comma separated list of result types. Allowed values are 'offer', 'venue' and 'advertisement'. */
@property(nonatomic, strong) NSString* include;

/* Optional */
@property(nonatomic, strong) NSDate* dateTime;

/* Optional, the number of weighted content items from the beginningg of the list to be returned */
@property(nonatomic, strong) NSNumber* offset;

/* Optional, limit, the maximum number of entries to be returned*/
@property(nonatomic, strong) NSNumber* limit;

/* Optional */
@property(nonatomic, strong) NSNumber* regionId;

/* YES, will return offers associated with a venue */
@property(nonatomic, assign) BOOL includeVenueRelatedOffers;

/* YES, the time filter will not be used by the search process */
@property(nonatomic, assign) BOOL ignoreDailyTimeFilter;

/* Optional, limit the results to this venueId */
@property(nonatomic, strong) NSNumber* filterByVenueId;
@end

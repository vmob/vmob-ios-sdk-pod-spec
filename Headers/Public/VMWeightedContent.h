//
//  VMWeightedContent.h
//  VMobSDK
//
//  Created by VMob on 13/05/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseModel.h"
#import "VMAdvertisement.h"
#import "VMOffer.h"
#import "VMVenue.h"
/**
 * VMob domain model - encapsulates weighted content of type Advertisement/Offer/Venue. This inherits from {@link #VMBaseModel}.
 */
@interface VMWeightedContent : VMBaseModel

/**
 * Content type.
 */
@property(nonatomic, strong) NSString* type;
/**
 * Content weight.
 */
@property(nonatomic, strong) NSNumber* weighting;
/**
 * Advertisement content.
 */
@property(nonatomic, strong) VMAdvertisement* advertisement;
/**
 * Offer content.
 */
@property(nonatomic, strong) VMOffer* offer;
/**
 * Venue content.
 */
@property(nonatomic, strong) VMVenue* venue;

@end

//
//  VMCoreLocationManager.h
//  VMobSDK
//
//  Created by VMob on 11/02/15.
//  Copyright (c) 2015 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "VMRequestManager.h"
#import <UIKit/UIKit.h>

extern NSString *const VMobLocationServiceAlertTitleKey;
extern NSString *const VMobLocationServiceAlertOkButtonTitleKey;
extern NSString *const VMobLocationServiceAlertCancelButtonTitleKey;
extern NSString *const VMobLocationServiceAlertSettingsButtonTitleKey;
extern NSString *const VMobLocationServiceDisabledMessageKey;
extern NSString *const VMobLocationCircularRegionUnavailableMessageKey;
extern NSString *const VMobLocationUnauthorizedMessageKey;

extern NSString *const VMobLocationServiceAlertTitle;
extern NSString *const VMobLocationServiceAlertOkButtonTitle;
extern NSString *const VMobLocationServiceAlertCancelButtonTitle;
extern NSString *const VMobLocationServiceAlertSettingsButtonTitle;
extern NSString *const VMobLocationServiceDisabledMessage;
extern NSString *const VMobLocationCircularRegionUnavailableMessage;
extern NSString *const VMobLocationUnauthorizedMessage;

typedef void (^BackgroundTaskBlock) (UIBackgroundTaskIdentifier indentifier);

@interface VMBaseLocationManager : VMRequestManager<CLLocationManagerDelegate> {
    @protected
        BackgroundFetchResultBlock _backgroundFetchResultBlock;
        BOOL _isServiceRunning;
}

/**
 * {@link #CLLocationManager} location manager.
 */
@property (readonly, nonatomic, strong) CLLocationManager* locationManager;

/**
 * Current {@link #CLLocation} location.
 */
@property (readonly, nonatomic, strong) CLLocation* currentLocation;

/**
 * Check whether location data has valid longitude, latitude.
 *
 * @param newLocation - location data to check.
 * @return whether it is a valid location data.
 */
- (BOOL) isValidLocation:(CLLocation *)newLocation;

/**
 * Execute tasks triggered by background fetch.
 *
 * @param backgroundFetchResultBlock - block forwarded from {@link #UIApplication} application:performFetchWithCompletionHandler:.
 */
- (void) executeBackgroundFetchWithHandler:(BackgroundFetchResultBlock) backgroundFetchResultBlock;

/**
 * Start location service.
 *
 * @return indicates whether successfully started location service.
 */
- (BOOL) startService;

/**
 * Stop location service.
 */
- (void) stopService;
@end

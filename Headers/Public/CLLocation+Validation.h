//
//  CLLocation+Validation.h
//  VMobSDK
//
//  Created by VMob on 11/11/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

/**
 * CLLocation category - Convenience methods for validation.
 */
@interface CLLocation (Validation)

/**
 * This method checks if a location object is valid, i.e it should have non-zero lat, long values.
 *
 * @return Whether it is a valid location data.
 */
- (BOOL)isValid;

@end

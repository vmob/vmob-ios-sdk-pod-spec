//
//  VMWeightedContentLite.h
//  VMobSDK
//
//  Created by VMob on 23/09/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMWeightedContent.h"
/**
 * VMob domain model - representing a subset of weighted content data of type Advertisement/Offer/Venue. This inherits from {@link #VMWeightedContent}.
 */
@interface VMWeightedContentLite : VMWeightedContent

@end

//
//  Utils.h
//  VMobSDK
//
//  Created by VMob on 5/29/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "VMConstant.h"

#define kBundleIdentifier [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleIdentifierKey]
#define kIsPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

/**
 * App id prefix.
 */
extern NSString *const kAppIdPrefix;
/**
 * Notification for receiving remote push notification.
 */
extern NSString *const VMobRemotePushReceivedNotification;

/**
 * VMob utility class - provides several convenience methods relevant to the SDK.
 */

@interface VMUtils : NSObject
/**
 * Current cached GPS location.
 */
@property (nonatomic, strong) CLLocation *currentLocation;
/**
 * Current facebook access token.
 */
@property (nonatomic, strong) NSString   *currentFacebookAccessToken;

#pragma mark - Token
/**
 * @return Current either device or consumer access token or null if neither does not exist.
 */
+ (NSString *) userToken;

#pragma mark - Other Methods
/**
 * @param string - Gender in String value i.e. m or f.
 * @return Gender in {@link #VMGender} converted from String value.
 */
+ (VMGender) genderTypeWithString: (NSString *) string;
/**
 * @param value - Boolean value.
 * @return Boolean in String converted from BOOL value.
 */
+ (NSString *) stringWithBOOL: (BOOL) value;
/**
 * @param socialType - social type.
 * @return Social type in String converted from {@link #VMSocialType} value.
 */
+ (NSString *) stringWithSocialToken:(VMSocialType) socialType;
/**
 * @param socialSource - social source type.
 * @return Social source type in String converted from {@link #VMSocialSource} value.
 */
+ (NSString *) stringWithSocialSource: (VMSocialSource) socialSource;
/**
 * @param gender - Gender in {@link #VMGender}.
 * @return Gender in String converted from {@link #VMGender} value.
 */
+ (NSString *) stringWithGrenderType: (VMGender) gender;
/**
 * Convert va_list to array.
 *
 * @param object - target object to be converted to array.
 * @param args - va_list arguments.
 * @return Array of data from va_list.
 */
+ (NSMutableArray *) arraWithInvoke: (NSObject *) object withParameters: (va_list) args;
/**
 * @param date - date object.
 * @return UTC date in String.
 */
+ (NSString *) UTCFormateDate: (NSDate *) date;
/**
 * @param date - date in String.
 * @return UTC Date object from String date.
 */
+ (NSDate *) dateUTCString: (NSString *) date;
/**
 * @return UTC date formatter.
 */
+ (NSDateFormatter *) UTCFormater;
/**
 * @return Singleton instance of VMUtils.
 */
+ (instancetype) sharedInstance;
/**
 * @return A new transaction id in UUID format.
 */
+ (NSString *) createTransactionId;
/**
 * @return App state in String description.
 */
+ (NSString*) descriptionForApplicationState;

#pragma mark - Header Helper
/**
 * @return device type either i_t for iPad or i_p for iPhone.
 */
+ (NSString *) deviceType;
/**
 * @return application version.
 */
+ (NSString *) applicationVersion;
/**
 * @return device resolution.
 */
+ (NSString *) deviceScreenResolution;
/**
 * @return carrier name.
 */
+ (NSString *) carrierName;
/**
 * @return name/TZID of system timezone.
 */
+ (NSString *) timezoneName;
/**
 * @return offset of UTC timezone.
 */
+ (NSString *) offsetUTC;
/**
 * @return connectivity type i.e.wifi, wwan or none.
 */
+ (NSString *) typeNetworkConnection;
/**
 * @return current cached location latitude.
 */
+ (NSString *) currentLatitude;
/**
 * @return current cached location longitude.
 */
+ (NSString *) currentLongitude;
/**
 * @return current cached location accuracy.
 */
+ (NSString *) currentAccuracy;
/**
 * @return current facebook access token.
 */
+ (NSString *) currentFacebookAccessToken;

#pragma mark - BaseURL API
/**
 * @return configuration API URL.
 */
+ (NSString *) configurationApiUrl;
/**
 * @return activity API URL.
 */
+ (NSString *) activityApiUrl;
/**
 * @return consumer API URL.
 */
+ (NSString *) consumerApiUrl;
/**
 * @return location API URL.
 */
+ (NSString *) locationApiUrl;
/**
 * @return offer API URL.
 */
+ (NSString *) offerApiUrl;
/**
 * @return offer image prefix URL.
 */
+ (NSString *) offerImagePrefixUrl;
/**
 * @return advertisement API URL.
 */
+ (NSString *) advertisementApiUrl;
/**
 * @return advertisement image prefix.
 */
+ (NSString *) advertisementImagePrefix;


#pragma mark - User Constant
/**
 * @returns whether the plist has configuration to autostart the SDK or not
 */
+ (BOOL)autoStartSDK;

/**
 * @return build version.
 */
+ (NSString *) buildVersion;
/**
 * @return unique device id.
 */
+ (NSString *) uniqueGlobalDeviceIdentifier;
/**
 * @return authorization key.
 */
+ (NSString *) authorizationKey;
/**
 * @return time inverval in seconds for sending activities.
 */
+ (NSTimeInterval) activitiesSendMinTimerInSeconds;
/**
 * @return activities batch size.
 */
+ (NSInteger) activitiesBatchSize;
/**
 * @return activities expiry size.
 */
+ (NSUInteger) activitiesExpireSize;
/**
 * @return activities expiry time inverval in seconds.
 */
+ (NSTimeInterval) activitiesExpireAgeInSeconds;
/**
 * @return whether auto-start beacon service.
 */
+ (BOOL) autostartBeaconService;
/**
 * @return whether auto-start geoFence service.
 */
+ (BOOL) autostartGeoFenceService;
/**
 * @return whether enable location service alert if location service either off or unauthorized
 */
+ (BOOL) enableLocationServiceAlert;
/**
 * @return time inverval for whether it should start background task process.
 */
+ (NSTimeInterval) gpsBackgroundMinIntervalInSeconds;
/**
 * @return time inverval used by {@link #VMLocationsManager} for sending activities.
 */
+ (NSTimeInterval) updateLocationTimeInterval;
/**
 * @return Current cached location accuracy.
 */
+ (double) locationAccuracy;
/**
 * @return Current cached location distance filter.
 */
+ (double) locationDistanceFilter;

#pragma mark - Configuration Methods
/**
 * @return Whether it auto-starts configuration update.
 */
+ (BOOL) addCodeForAutoConfgurationUpdate;
/**
 * @return Whether it auto-starts location service.
 */
+ (BOOL) addCodeForAutoLocationServices;
/**
 * @return Time interval in minutes for logging app start activity when app becomes active from background state. 
 *         Minimum value is 30 mins.
 */
+ (NSTimeInterval) startupActivityInterval;
/**
 * @return Whether it updates configuration.
 */
+ (BOOL) isUpdateConfiguration;
/**
 * @return Last update time for configuration.
 */
+ (NSDate *) lastUpdateConfiguration;
/**
 * Update timestamp of configuration.
 */
+ (void) updateConfiguration;
/**
 * Add value to overridden config dictionary in order to overwrite some config values.
 *
 * @param overrideValue - new value.
 * @param configurationKey - config key.
 */
+ (void) setInfoPlistOverride: (id) overrideValue forKey: (NSString *) configurationKey;

#pragma mark - Dev Utils

#pragma mark - APNS environment check
/**
 * @return APNS environment.
 */
+ (NSString *)getAPNSEnvironment;

#pragma mark - logging support
/**
 * @return whether logging is enabled.
 */
+ (BOOL)isLoggingEnabled;
/**
 * @return logging level.
 */
+ (NSString *)loggingLevel;
/**
 * @return whether logging saved to file.
 */
+ (BOOL)isLoggingToFile;

#pragma mark - tool
/**
 * Assert test whether a key-value pair exists in the info plist.
 *
 * @param aKeyName - plist key name.
 */
+(void)assertExistenceOfRequiredPlistEntries:(NSString *)aKeyName;


/**
 * Convenience method for preparing CDN formatted image url with a given dimension.
 *
 * @param anImagePath - image name - must not be null
 * @param aWidth - can be either 0 or greater
 * @param aHeight - can be either 0 or greater
 * @param anImageFormat - can be nil or use either VMImageFormatPNG or VMImageFormatJPEG
 * @param anImageUrlPrefix - use either [VMUtils advertisementImagePrefix] or [VMUtils offerImagePrefixUrl] depending on the context
 *
 * @return nil or absolute CDN image url path 
 */
+ (NSString *)imageUrlStringWithPath:(NSString *)anImagePath width:(int)aWidth height:(int)aHeight imageFormat:(NSString *)anImageFormat andUrlPrefix:(NSString *)anImageUrlPrefix;

@end

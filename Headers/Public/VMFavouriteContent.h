//
//  VMFavouriteContent.h
//  VMobSDK
//
//  Created by VMob on 2/06/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseModel.h"
/**
 * Encapsulates consumer's favorite content - Advertisements, Offer, Merchants and Venues.
 * Inherits from {@link #VMBaseModel}
 */
@interface VMFavouriteContent : VMBaseModel
/**
 * The list of favourite offers.
 */
@property(nonatomic, strong) NSArray* offers;
/**
 * The list of favourite advertisements.
 */
@property(nonatomic, strong) NSArray* advertisements;
/**
 * The list of favourite merchants.
 */
@property(nonatomic, strong) NSArray* merchants;
/**
 * The list of favourite venues.
 */
@property(nonatomic, strong) NSArray* venues;

@end

//
//  VMVenue.h
//  VMobSDK
//
//  Created by VMob on 13/05/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMContentModel.h"
#import "VMVenueOpenHours.h"
#import "VMOffer.h"
/**
 * VMob domain model - encapsulates Venue specific data. This inherits from {@link #VMContentModel}.
 */
@interface VMVenue : VMContentModel
/**
 * Venue name.
 */
@property(nonatomic, strong) NSString* name;
/**
 * Venue address.
 */
@property(nonatomic, strong) NSString* address;
/**
 * Venue phone number.
 */
@property(nonatomic, strong) NSString* phone;
/**
 * Venue email.
 */
@property(nonatomic, strong) NSString* email;
/**
 * Venue web.
 */
@property(nonatomic, strong) NSString* web;
/**
 * Venue post code.
 */
@property(nonatomic, strong) NSString* postCode;
/**
 * Venue city.
 */
@property(nonatomic, strong) NSString* city;
/**
 * Venue distance from user current location.
 */
@property(nonatomic, strong) NSNumber* distanceFromCurrentLocation;
/**
 * Venue external id.
 */
@property(nonatomic, strong) NSString* externalId;
/**
 * Venue Region id.
 */
@property(nonatomic, strong) NSNumber* regionId;
/**
 * Venue GPS latitude.
 */
@property(nonatomic, strong) NSNumber* latitude;
/**
 * Venue GPS longitude.
 */
@property(nonatomic, strong) NSNumber* longitude;
/**
 * The list of venue opening hours.
 */
@property(nonatomic, strong) NSArray* venueOpenHours;
/**
 * The list of venue features.
 */
@property(nonatomic, strong) NSArray* features;
/**
 * The list of venue offers.
 */
@property(nonatomic, strong) NSArray* offers;

@end

//
//  NSString+Additions.h
//  Mobile
//  Created by VMob on 17.09.10.
//  Copyright 2010 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * NSString category - Convenience methods for string manipulation
 */
@interface NSString(VMAdditions)
/**
 * @param format - data format used by {@link #NSDateFormatter}.
 * @return Converted date object from String value.
 */
- (NSDate *) dateValueWithFormat: (NSString *) format;

/**
 * @param index - index position.
 * @return A specified letter in specified index position.
 */
- (NSString *) charAtIndex: (int) index;

/**
 * @param str - target search String value.
 * @return Whether contains the String value.
 */
- (BOOL) containsString:(NSString*) str;

/**
 * @return Dictionary representation of JSON data.
 */
- (NSDictionary *)toDictionaryFromJSONString;


@end

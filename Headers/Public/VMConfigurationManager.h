//
//  VMConfigurationManager.h
//  VMobSDK
//
//  Created by VMob on 7/8/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import "VMRequestManager.h"

/**
 * VMConfigurationManager is a subclass of {@link #VMRequestManager}, provided to retrieve platform configurations.
 */
@interface VMConfigurationManager : VMRequestManager

/**
 * Configuration categories.
 */
@property (nonatomic, strong) NSMutableArray *categories;

/**
 * The shared locations manager object for the system.
 *
 * @return The systemwide locations manager.
 */
+ (instancetype) sharedInstance;

/**
 * @return Configuration data.
 */
- (NSDictionary *) configuration;

/**
 * Update local configuration data with platform response.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSDictionary* or NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *)updateConfigurationWithCompletionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Get platform configuration data.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSDictionary* or NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *)getConfigurationWithCompletionBlock: (VMobCompletionBlock) completionBlock;

/**
 * @return Last loaded configuration file; OR returns nil if the configuration parameters have not been loaded from the API yet.
 */
+ (NSDictionary *) lastConfiguration;

@end

//
//  VMRedeemedOffer.h
//  VMobSDK
//
//  Created by VMob on 21/11/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMContentModel.h"
#import "VMConstant.h"

/**
 * VMob domain model - encapsulates data related to a redeemed Offer. This inherits from {@link #VMContentModel}.
 */
@interface VMRedeemedOffer : VMContentModel

/**
 * Redeemed offer id.
 */
@property(nonatomic, strong) NSNumber * offerId;

/**
 * Redemption text. Please check the redemptionExpiryDate in case there is any expiry date associated with redemption.
 */
@property(nonatomic, strong) NSString * redemptionText;

/**
 * Redemption expiry date, if the associated redemption has an expiry date.
 */
@property(nonatomic, strong) NSDate* redemptionExpiryDate;


/**
 * Redeemed offer terms and conditions.
 */
@property(nonatomic, strong) NSString * termsAndConditions;

/**
 * Redeemed offer last update time.
 */
@property(nonatomic, strong) NSDate * lastUpdatedAt;

/**
 * Redeemed offer gift batch id.
 */
@property(nonatomic, strong) NSString * giftBatchId;

/**
 * Redeemed offer gift id.
 */
@property(nonatomic, strong) NSString * giftId;

/**
 * Redeemed offer gifted by consumer id.
 */
@property(nonatomic, strong) NSString * giftedByConsumerId;

/**
 * Redeemed offer gifted on date.
 */
@property(nonatomic, strong) NSDate * giftedOnDate;

/**
 * Whether redeemed offer is a gift.
 */
@property(nonatomic, strong) NSNumber * isAGift;

/**
 * Redeemed offer instance unique id.
 */
@property(nonatomic, strong) NSString * offerInstanceUniqueId;

/**
 * The list of venue ids.
 */
@property(nonatomic, strong) NSArray * venueIds;

/**
 * Whether offer is burnt.
 */
@property(nonatomic, strong) NSNumber * isBurnt;

/**
 * Whether offer is reward.
 */
@property(nonatomic, strong) NSNumber * isReward;

/**
 * Offer redemption type.
 */
@property(nonatomic, readwrite) VMOfferRedemptionType redemptionType;

/**
 * Offer redeemded at time.
 */
@property(nonatomic, strong) NSDate* redeemedAt;

/**
 * Offer burned at time.
 */
@property(nonatomic, strong) NSDate* burntAt;


@end

//
//  VMOffer.h
//  VMobSDK
//
//  Created by VMob on 13/05/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMContentModel.h"
#import "VMVenue.h"
#import "VMConstant.h"

/**
 * Enum that lists all the possible fields inside an {@link VMOffer}. This is useful for
 * methods that need the developers to specify which fields of an {@link VMOffer} must be returned
 * on a response when sending a request to the server, such as in
 * {@link VMOfferSearchCriteria#fields}.
 */
extern const struct OfferFieldStruct {
    __unsafe_unretained NSString * const ID;
    __unsafe_unretained NSString * const Title;
    __unsafe_unretained NSString * const Description;
    __unsafe_unretained NSString * const StartDate;
    __unsafe_unretained NSString * const EndDate;
    __unsafe_unretained NSString * const MerchantId;
    __unsafe_unretained NSString * const Image;
    __unsafe_unretained NSString * const AlternativeImage;
    __unsafe_unretained NSString * const IsMerchantFavourite;
    __unsafe_unretained NSString * const LastUpdatedAt;
    __unsafe_unretained NSString * const RedemptionCount;
    __unsafe_unretained NSString * const CategoryID;
    __unsafe_unretained NSString * const CategoryName;
    __unsafe_unretained NSString * const SortOrder;
    __unsafe_unretained NSString * const IsPremiumPlacement;
    __unsafe_unretained NSString * const PaymentType;
    __unsafe_unretained NSString * const PaymentAmount;
    __unsafe_unretained NSString * const PaymentTaxRate;
    __unsafe_unretained NSString * const RedemptionType;
    __unsafe_unretained NSString * const ExtendedData;
    __unsafe_unretained NSString * const Weight;
    __unsafe_unretained NSString * const DailyStartTime;
    __unsafe_unretained NSString * const DailyEndTime;
    __unsafe_unretained NSString * const DaysOfWeek;
    __unsafe_unretained NSString * const IsGiftable;
    __unsafe_unretained NSString * const IsGift;
    __unsafe_unretained NSString * const GiftedBy;
    __unsafe_unretained NSString * const GiftedCopy;
    __unsafe_unretained NSString * const GiftedID;
    __unsafe_unretained NSString * const GiftBatchID;
    __unsafe_unretained NSString * const GiftedDate;
    __unsafe_unretained NSString * const OfferInstanceUniqueId;
    __unsafe_unretained NSString * const IsActive;
    __unsafe_unretained NSString * const TermsAndConditions;
    __unsafe_unretained NSString * const IsReward;
    __unsafe_unretained NSString * const BurntCount;
    __unsafe_unretained NSString * const DistanceToClosestVenue;
    __unsafe_unretained NSString * const ClosestVenue;
    __unsafe_unretained NSString * const IsAvailableAllStores;
    __unsafe_unretained NSString * const VenuesIDs;
    __unsafe_unretained NSString * const VenuesExternalIDs;
    __unsafe_unretained NSString * const ContentTagReferenceCodes;
    __unsafe_unretained NSString * const ContentURL;
    __unsafe_unretained NSString * const IsRespawning;
    __unsafe_unretained NSString * const RespawnsInDays;
    __unsafe_unretained NSString * const LastRedeemedAt;
    __unsafe_unretained NSString * const LastBurntAt;
} OfferField;

@class VMVenue;

/**
 * VMob domain model - encapsulates Offer/Coupon specific data. This inherits from {@link #VMContentModel}.
 */
@interface VMOffer : VMContentModel
/**
 * Offer burnt count.
 */
@property(nonatomic, strong) NSNumber* burntCount;
/**
 * Offer category id.
 */
@property(nonatomic, strong) NSNumber* categoryId;
/**
 * Offer category name. 
 */
@property(nonatomic, strong) NSString* categoryName;

/**
 * Offer redemption type.
 */
@property(nonatomic, readwrite) VMOfferRedemptionType redemptionType;

/**
 * Offer content URL.
 */
@property(nonatomic, strong) NSString* contentUrl;
/**
 * Offer availble days in week.
 */
@property(nonatomic, strong) NSArray* daysOfWeek;
/**
 * Offer gift batch id.
 */
@property(nonatomic, strong) NSNumber* giftBatchId;
/**
 * Offer gift id.
 */
@property(nonatomic, strong) NSNumber* giftId;
/**
 * Offer gift by.
 */
@property(nonatomic, strong) NSString* giftedBy;
/**
 * Offer gift by consumer id.
 */
@property(nonatomic, strong) NSString* giftedByConsumerId;
/**
 * Offer gifted copy.
 */
@property(nonatomic, strong) NSString* giftedCopy;
/**
 * Offer gifted on date.
 */
@property(nonatomic, strong) NSDate* giftedOnDate;
/**
 * Offer alernative image.
 */
@property(nonatomic, strong) NSString* imageAlt;
/**
 * Whether offer is a gift.
 */
@property(nonatomic, strong) NSNumber* isAGift;
/**
 * Whether offer is active.
 */
@property(nonatomic, strong) NSNumber* isActive;
/**
 * Whether offer is giftable.
 */
@property(nonatomic, strong) NSNumber* isGiftable;
/**
 * Whether offer is merchant favourite.
 */
@property(nonatomic, strong) NSNumber* isMerchantFavourite;
/**
 * Whether offer is premium placement.
 */
@property(nonatomic, strong) NSNumber* isPremiumPlacement;
/**
 * Whether offer is respawning.
 */
@property(nonatomic, strong) NSNumber* isRespawningOffer;
/**
 * Whether offer is reward.
 */
@property(nonatomic, strong) NSNumber* isReward;
/**
 * Offer last update time.
 */
@property(nonatomic, strong) NSDate* lastUpdatedAt;
/**
 * Offer instance unique id.
 */
@property(nonatomic, strong) NSString* offerInstanceUniqueId;
/**
 * Offer payment amount.
 */
@property(nonatomic, strong) NSNumber* paymentAmount;
/**
 * Offer payment tax rate.
 */
@property(nonatomic, strong) NSNumber* paymentTaxRate;
/**
 * Offer payment type.
 */
@property(nonatomic, strong) NSNumber* paymentType;
/**
 * Offer redemption count.
 */
@property(nonatomic, strong) NSNumber* redemptionCount;
/**
 * The number of days for respawning an offer.
 */
@property(nonatomic, strong) NSNumber* respawnsInDays;
/**
 * Offer sorting order.
 */
@property(nonatomic, strong) NSNumber* sortOrder;
/**
 * Offer sub-category id.
 */
@property(nonatomic, strong) NSNumber* subCategoryId;
/**
 * Offer sub-category name.
 */
@property(nonatomic, strong) NSString* subCategoryName;
/**
 * Offer terms and conditions.
 */
@property(nonatomic, strong) NSString* termsAndConditions;
/**
 * Distance to closet venue which has the offer.
 */
@property(nonatomic, strong) NSNumber* distanceToClosestVenue;
/**
 * Closet venue which has the offer.
 */
@property(nonatomic, strong) VMVenue* closestVenue;
/**
 * The list of venue ids which have the offer.
 */
@property(nonatomic, strong) NSArray* venueIds;
/**
 * The list of venue external ids.
 */
@property(nonatomic, strong) NSArray* venueExternalIds;
/**
 * The list of content tag reference codes.
 */
@property(nonatomic, strong) NSArray* contentTagReferenceCodes;


/**
 * Offer last redeemded at time.
 */
@property(nonatomic, strong) NSDate* lastRedeemedAt;

/**
 * Offer last burned at time.
 */
@property(nonatomic, strong) NSDate* lastBurntAt;

/**
 * @return the full image url with the right CDN prefix.
 */
- (NSString *)absoluteImageUrlString;
/**
 * @param aWidth - image width.
 * @param aHeight - image height.
 *
 * @return the full image url with the right CDN prefix with specified width and height.
 */
- (NSString *)absoluteImageUrlStringWithWidth:(int)aWidth andHeight:(int)aHeight;
/**
 * @return the full image url with the right CDN prefix in JEPG format.
 */
- (NSString *)absoluteJpegImageUrlString;
/**
 * @param aWidth - image width.
 * @param aHeight - image height.
 *
 * @return the full image url with the right CDN prefix with specified width and height in in JEPG format.
 */
- (NSString *)absoluteJpegImageUrlStringWithWidth:(int)aWidth andHeight:(int)aHeight;

/**
 * @return the full alt image url with the right CDN prefix.
 */
- (NSString *)absoluteImageAltUrlString;
/**
 * @param aWidth - image width.
 * @param aHeight - image height.
 *
 * @return the full alt image url with the right CDN prefix with specified width and height.
 */
- (NSString *)absoluteImageAltUrlStringWithWidth:(int)aWidth andHeight:(int)aHeight;
/**
 * @return the full alt image url with the right CDN prefix in JEPG format.
 */
- (NSString *)absoluteJpegImageAltUrlString;
/**
 * @param aWidth - image width.
 * @param aHeight - image height.
 *
 * @return the full alt image url with the right CDN prefix with specified width and height in in JEPG format.
 */
- (NSString *)absoluteJpegImageAltUrlStringWithWidth:(int)aWidth andHeight:(int)aHeight;

@end

//
//  VMActivity.h
//  VMobSDK
//
//  Created by VMob on 5/29/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import "VMRequestManager.h"
#import "VMConstant.h"
#import <CoreLocation/CoreLocation.h>
#import "VMBeacon.h"

/**
 * VMActivityManager is a subclass of {@link #VMRequestManager}, provided to interact with VMob Activities.
 */
@interface VMActivityManager : VMRequestManager

/**
 * @return Current activity stack in the VMActivityManager class.
 */

- (NSArray *) stackActivity;

/**
 * @return Timestamp for the last successful sent stack in the `VMActivityManager` class.
 */

@property (nonatomic, readonly) NSDate *lastSendStack;

/**
 * The shared activity manager object for the system.
 *
 * @return The systemwide activity manager.
 */

+ (instancetype) sharedInstance;

#pragma mark - Follow Methods
/**
 * Sets block called when sending the activity stack (returns NSDate).
 *
 * @param block - block called when sending the activity stack. If nil is passed here, parameter will be nil. Data parameter return as NSDate* datatype.
*/

- (void) setUpdateDate: (VMobCompletionBlock) block;

/**
 * Sets block that is called when the activity stack is updated (returns NSArray).
 *
 * @param block - block called when the activity stack is updated (returns NSArray). If nil is passed here, parameter will be nil. Data parameter return as NSMutableArray* datatype.
*/

- (void) setUpdateStackActivity: (VMobCompletionBlock) block;

#pragma mark - Activity API Methods

/**
 * Method is provided to log an instance of a Client Application 'starting', e.g. coming out of hibernation or cold start.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/

- (VMOperation *) sendHistoryActivityWithCompletionBlock: (VMobCompletionBlock) completionBlock;

#pragma mark - ButtonClick Methods

/**
 * The Button Click type is provided to log an instance of a Consumer clicking a button within the Client Application.
 *
 * @param buttonId - unique identifier for the button which was clicked.
 * @param merchantId - ID of the merchant (should only be supplied if relevant to the click).
 * @param venueId - ID of the venue (should only be supplied if relevant to the click).
 * @param itemId - ID of the item (should only be supplied if relevant to the click).
 * @param itemCode - identifies the type for the itemId, e.g. if itemId is an offer id then itemCode must be 'O'. itemCode is mandatory when itemId contains a value.
 */

- (void) buttonClickWithButtonId: (NSString *) buttonId
					  merchantId: (NSString *) merchantId
						 venueId: (NSString *) venueId
						  itemId: (NSString *) itemId
						itemCode: (NSString *) itemCode;

/**
 * The Button Click type is provided to log an instance of a Consumer clicking a button within the Client Application with extra action values.
 *
 * @param buttonId - unique identifier for the button which was clicked.
 * @param merchantId - ID of the merchant (should only be supplied if relevant to the click).
 * @param venueId - ID of the venue (should only be supplied if relevant to the click).
 * @param itemId - ID of the item (should only be supplied if relevant to the click).
 * @param itemCode - identifies the type for the itemId, e.g. if itemId is an offer id then itemCode must be 'O'. itemCode is mandatory when itemId contains a value.
 * @param actionvalue1 - custom field1.
 * @param actionvalue2 - custom field2.
 * @param actionvalue3 - custom field3.
 **/

- (void) buttonClickWithButtonId: (NSString *) buttonId
                      merchantId: (NSString *) merchantId
                         venueId: (NSString *) venueId
                          itemId: (NSString *) itemId
                        itemCode: (NSString *) itemCode
                    actionvalue1: (NSString *)actionvalue1
                    actionvalue2: (NSString *)actionvalue2
                    actionvalue3: (NSString *)actionvalue3;

#pragma mark - LocationCheckin Methods
/**
 * The Location Checkin type is provided to log the current location of the consumer/device.
 *
 * @param typeGPS - this is where/how the GPS information was gathered from on the device.
 * @param location - this is CLLocation.
 */

- (void) locationCheckinWithTypeGPS: (VMTypeGPS) typeGPS
						   location: (CLLocation *) location;
/**
 * The Location Checkin type is provided to log the current location of the consumer/device
 *
 * @param typeGPS - this is where/how the GPS information was gathered from on the device.
 * @param location - this is CLLocation.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
*/
- (void) locationCheckinWithTypeGPS: (VMTypeGPS) typeGPS
                           location: (CLLocation *) location
                         geoFenceId: (NSString *) geoFenceId
                    completionBlock: (VMobCompletionBlock) completionBlock;

#pragma mark - AppPageImpression Methods
/**
 * The AppPageImpression type is provided to log an instance of a Consumer viewing a page within the Client Application.
 *
 * @param pageId - unique identifier to the page which was viewed.
 * @param merchantId - ID of the merchant (should only be supplied if relevant).
 * @param venueId - ID of the venue (should only be supplied if relevant).
 * @param itemId - ID of the item (should only be supplied if relevant).
 * @param itemCode - identifies the type for the itemId, e.g. if itemId is an offer id then itemCode must be 'O'. itemCode is mandatory when itemId contains a value.
 */

- (void) appPageImpressionWithPageId: (NSString *) pageId
						  merchantId: (NSString *) merchantId
							 venueId: (NSString *) venueId
							  itemId: (NSString *) itemId
							itemCode: (NSString *) itemCode;

#pragma mark - Offer Methods
/**
 * The OfferImpression type is provided to log an instance of an offer coming into the viewable area of the Client Application.
 *
 * @param placementType - the placement type it originated from.
 * @param offerId - ID of the offer that was seen.
 */

- (void) offerImpressionWithPlacementType: (NSString *) placementType
								  offerId: (NSString *) offerId;

/**
 * The OfferClickThrough type is provided to log an instance of a Consumer clicking on an offer to view the full detail within the Client Application.
 *
 * @param placementType - the placement type it originated from.
 * @param offerId - ID of the offer that was seen.
 */

- (void) offerClickThroughWithPlacementType: (NSString *) placementType
									offerId: (NSString *) offerId;


/**
 * The LoyaltyCardImpression type is provided to log an instance of a LoyaltyCard coming into the viewable area of the Client Application.
 *
 * @param loyaltyCardId - ID of the loyalty card that was seen (NOT the loyaltyCardInstanceId).
 */

- (void) loyaltyCardImpressionWithLoyaltyCardId:(NSString *)loyaltyCardId;


#pragma mark - Social Methods
/**
 * The Offer Share type is provided to log an instance of a Consumer sharing an offer via a 3rd party application.
 *
 * @param socialType - the type of login that has occurred.
 * @param offerId - ID of the offer that was seen.
 */

- (void) offerShareWithSocialType: (VMShareSocialType) socialType
						  offerId: (NSString *) offerId;

#pragma mark - Advertisement Methods
/**
 * The Advertisement Impression type is provided to log an instance of an being displayed to the consumer on the app.
 *
 * @param placementCode - the placementCode for the advertisement.
 * @param channelCode - the channelCode for the advertisement.
 * @param advertisementId - ID of the advertisement that was seen.
 */

- (void) advertisementImpressionWithPlacementCode: (NSString *) placementCode
									  channelCode: (NSString *) channelCode
								  advertisementId: (NSString *) advertisementId;

/**
 * The Advertisement Click type is provided to log an instance of a Consumer clicking on an advertisement within the application.
 *
 * @param placementCode - the placementCode for the advertisement.
 * @param channelCode - the channelCode for the advertisement.
 * @param advertisementId - ID of the advertisement that was clicked.
 */

- (void) advertisementClickWithPlacementCode: (NSString *) placementCode
								 channelCode: (NSString *) channelCode
							 advertisementId: (NSString *) advertisementId;

/**
 * The OfferImpression type is provided to log an instance of an offer coming into the viewable area of the Client Application with extra action values.
 *
 * @param placementType - the placement type it originated from.
 * @param offerId - ID of the offer that was seen.
 * @param actionvalue1 - custom field1.
 * @param actionvalue2 - custom field2.
 * @param actionvalue3 - custom field3.
 */

- (void) offerImpressionWithPlacementType: (NSString *) placementType
								  offerId: (NSString *) offerId
                             actionvalue1: (NSString *)actionvalue1
                             actionvalue2: (NSString *)actionvalue2
                             actionvalue3: (NSString *)actionvalue3;

/**
 * The OfferClickThrough type is provided to log an instance of a Consumer clicking on an offer to view the full detail within the Client Application with extra action values.
 * 
 * @param placementType - the placement type it originated from.
 * @param offerId - ID of the offer that was clicked.
 * @param actionvalue1 - custom field1.
 * @param actionvalue2 - custom field2.
 * @param actionvalue3 - custom field3.
 */
- (void) offerClickThroughWithPlacementType: (NSString *) placementType
									offerId: (NSString *) offerId
                               actionvalue1: (NSString *)actionvalue1
                               actionvalue2: (NSString *)actionvalue2
                               actionvalue3: (NSString *)actionvalue3;

#pragma mark - beacon support
/**
 * Sends a "beacon check in" activity to the server.
 * NOTES: SDK automatically call this method when detect beacon in the region.
 *
 * @param beacon - beacon information e.g.beacon uuid which is in the region.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
*/
- (void)checkinWithBeacon:(VMBeacon *)beacon completionBlock: (VMobCompletionBlock) completionBlock ;

/**
 * Sends a "beacon check out" activity to the server.
 * NOTES: SDK DOES NOT automatically call this method when detect beacon out of the region.
 *
 * @param beacon - beacon information e.g.beacon uuid which is out of the region.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error
    that occurred during the request.
*/
- (void)checkoutWithBeacon:(VMBeacon *)beacon completionBlock: (VMobCompletionBlock) completionBlock ;

#pragma mark - push notification support

/**
 * Sends a "clicked push message" activity to the server.
 * NOTES: SDK automatically call this method when app launches with launchOptions UIApplicationLaunchOptionsRemoteNotificationKey available.
 *
 * @param aMessageId - message id comes from remote notification payload.
 */
- (void)pushMessageClickedWithId:(NSString *)aMessageId;


@end

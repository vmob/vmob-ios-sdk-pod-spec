//
//  VMConsumer.h
//  VMobSDK
//
//  Created by VMob on 5/31/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMConstant.h"
#import "VMBaseModel.h"

/**
 * VMob domain model - encapsulates Consumer specific data. This inherits from {@link #VMBaseModel}.
 */
@interface VMConsumer : VMBaseModel

/**
 * Consumer's username.
 */
@property (nonatomic, strong) NSString *userName;

/**
 * Consumer's last name.
 */
@property (nonatomic, strong) NSString *lastName;

/**
 * Consumer's full name.
 */
@property (nonatomic, strong) NSString *fullName;

/**
 * Consumer's first name.
 */
@property (nonatomic, strong) NSString *firstName;

/**
 * Consumer's home city.
 */
@property (nonatomic, strong) NSString *homeCity;

/**
 * Consumer's email address.
 */
@property (nonatomic, strong) NSString *emailAddress;

/**
 * Consumer's date of birth.
 */
@property (nonatomic, strong) NSDate *dateOfBirth;

/**
 * Consumer's gender.
 */
@property (nonatomic) VMGender gender;

/*
 * Any extended data passed in, mostly JSON serialized string.
 */
 
@property (nonatomic, strong) NSString *extendedData;



@end

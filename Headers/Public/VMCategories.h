//
//  VMCategories.h
//  VMobSDK
//
//  Created by VMob on 7/23/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseModel.h"

/**
 * VMCategories is a categories class.
 */
@interface VMCategories : VMBaseModel

/**
 * ID of the category.
 */
@property (nonatomic, strong) NSNumber *categoryId;

/**
 * Name of the Category.
 */
@property (nonatomic, strong) NSString *name;

/**
 * SortOrder applied for the category.
 */
@property (nonatomic, strong) NSNumber *sortOrder;

/**
 * Hi-resolution image for the category.
 */
@property (nonatomic, strong) NSString *highResolutionImage;

/**
 * Hi-resolution Selected Image for the category.
 */
@property (nonatomic, strong) NSString *highResolutionSelectedImage;

/**
 * Low-resolution Image for the category.
 */
@property (nonatomic, strong) NSString *lowResolutionImage;

/**
 * Low-resolution Selected Image for the category.
 */
@property (nonatomic, strong) NSString *lowResolutionSelectedImage;

@end

//
//  VMConsumer.h
//  VMobSDK
//
//  Created by VMob on 5/30/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import "VMRequestManager.h"
#import "VMConsumer.h"
#import "VMConstant.h"

/**
 * VMConsumerManager is a subclass of {@link #VMRequestManager}, provided for managing all interactions applicable to Consumers in VMob platform.  This class can be used to retrieve consumer details, update details, tag consumer and more
 */
@interface VMConsumerManager : VMRequestManager

/**
 * The shared consumer manager object for the system.
 *
 * @return The systemwide consumer manager.
 */
+ (instancetype) sharedInstance;

#pragma mark - Detail Methods
/**
 * Gets the current consumer details.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as VMConsumer* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) detailConsumerWithCompletionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Updates a subset of consumer properties.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param consumer - Updated {@link #VMConsumer} object.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) updateConsumerWithConsumer: (VMConsumer *) consumer
                             completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Updates a subset of consumer properties with a verification token
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param consumer - Updated {@link #VMConsumer} object.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) updateConsumerWithConsumer: (VMConsumer *) consumer
                           verificationToken: (NSString *)verificationToken
                             completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Updates a subset of consumer properties.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param userName - User name is used for login.
 * @param firstName - Updated consumer's first name. Either both the `firstName` and `lastName` properties or just the `fullName` must be supplied, it is not necessary to supply both.
 * @param lastName - Updated consumer's last name. Either both the `firstName` and `lastName` properties or just the `fullName` must be supplied, it is not necessary to supply both.
 * @param fullName - Updated consumer's full name. Either both the `firstName` and `lastName` properties or just the `fullName` must be supplied, it is not necessary to supply both.
 * @param gender - Updated consumer's gender.
 * @param dateOfBirth - Updated consumer's date of birth.
 * @param city - Updated ID of the city where the consumer lives. Home City is a new property, if not implemented on the client application yet, please specify as '2'.
 * @param emailAddress - Updated email address.
 * @param extendedData - Updated extra data in String JSON dictionary string representation
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) updateConsumerWithUserName: (NSString *) userName
                                   firstName: (NSString *) firstName
                                    lastName: (NSString *) lastName
                                    fullName: (NSString *) fullName
                                      gender: (VMGender) gender
                                 dateOfBirth: (NSDate *) dateOfBirth
                                        city: (NSString *) city
                                emailAddress: (NSString *) emailAddress
                                extendedData: (NSString *) anExtendedData
                             completionBlock: (VMobCompletionBlock) completionBlock;


/**
 * Updates a subset of consumer properties with a verification token
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param userName - User name is used for login.
 * @param firstName - Updated consumer's first name. Either both the `firstName` and `lastName` properties or just the `fullName` must be supplied, it is not necessary to supply both.
 * @param lastName - Updated consumer's last name. Either both the `firstName` and `lastName` properties or just the `fullName` must be supplied, it is not necessary to supply both.
 * @param fullName - Updated consumer's full name. Either both the `firstName` and `lastName` properties or just the `fullName` must be supplied, it is not necessary to supply both.
 * @param gender - Updated consumer's gender.
 * @param dateOfBirth - Updated consumer's date of birth.
 * @param city - Updated ID of the city where the consumer lives. Home City is a new property, if not implemented on the client application yet, please specify as '2'.
 * @param emailAddress - Updated email address.
 * @param extendedData - Updated extra data in String JSON dictionary string representation
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) updateConsumerWithUserName: (NSString *) userName
                                   firstName: (NSString *) firstName
                                    lastName: (NSString *) lastName
                                    fullName: (NSString *) fullName
                                      gender: (VMGender) gender
                                 dateOfBirth: (NSDate *) dateOfBirth
                                        city: (NSString *) city
                                emailAddress: (NSString *) emailAddress
                                extendedData: (NSString *) anExtendedData
                           verificationToken: (NSString *)verificationToken
                             completionBlock: (VMobCompletionBlock) completionBlock;



/**
 * Makes a consumer account an anonymous account.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/

- (VMOperation *)anonymizeAccountWithCompletionBlock:(VMobCompletionBlock)completionBlock;

/**
 * Removes the account completely.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/

- (VMOperation *)deleteConsumerWithCompletionBlock:(VMobCompletionBlock)completionBlock __attribute((deprecated("Use methods deleteConsumerAccountWithCompletionBlock, deleteDeviceAccountWithCompletionBlock accordingly instead.")));

/**
 * Removes the consumer account completely if the user is logged in.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */

- (VMOperation *)deleteConsumerAccountWithCompletionBlock:(VMobCompletionBlock)completionBlock;

/**
 * Removes the device account completely if the user is not logged in.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
 */

- (VMOperation *)deleteDeviceAccountWithCompletionBlock:(VMobCompletionBlock)completionBlock;

#pragma mark - Tags Methods

/**
 * Gets the list of the tag values that are assigned to the current consumer. The string value returned for each tag is the referenceCode of tag value (unique text code).
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) listTagWithCompletionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Attempts to add a range of tag values to a consumer. Any invalid or duplicate values will fail silently.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param tagValueReferenceCodes - tags Value Reference Codes to be added.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) addTagValueReferenceCodes: (NSArray *) tagValueReferenceCode
                            completionBlock: (VMobCompletionBlock) completionBlock;
/**
 * Attempts to remove a range of tag values from the consumer. Any invalid or missing values will fail silently.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param tagValueReferenceCodes - tags Value Reference Codes to be removed.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) removeTagValueReferenceCodes: (NSArray *) tagValueReferenceCode
                               completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Attempts to add a range of tag values to a consumer and remover a range of tag values from a consumer. Any invalid or duplicate values will fail silently.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param tagValueAddReferenceCodes - tags Value Reference Codes to be updated.
 * @param tagValueRemoveReferenceCodes - tags Value Reference Codes to be removed.
 * @param completionBlock The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/

- (VMOperation *) updateTagWithTagValueAddReferenceCodes: (NSArray *) tagValueAddReferenceCodes
                            tagValueRemoveReferenceCodes: (NSArray *) tagValueRemoveReferenceCodes
                                         completionBlock: (VMobCompletionBlock) completionBlock;


#pragma mark Social connection methods
/**
 * Creates or updates the social connection of the account.
 * If a social connection of the same type already exists, it will remove it and add this new one.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param socialSource - the type of social connection.
 * @param credential - the credentials of the connection. For example the facebook accessToken.
 * @param updateConsumerInfo - whether should update consumer information about social source.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/

- (VMOperation *) createUpdateSocialConnectionWithSocialSource: (VMSocialSource) socialSource
                                                    credential: (NSString *) credential
                                            updateConsumerInfo: (BOOL) updateConsumerInfo
                                               completionBlock: (VMobCompletionBlock) completionBlock __attribute((deprecated("This method should now be accessed using VMAuthorizationManager")));

/**
 * Change consumer account to anonymous.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param FBtoken - Facebook session token.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) changeConsumerFacebookAccountToAnonymousWithFBSessionToken:(NSString *)FBtoken
                                                             completionBlock:(VMobCompletionBlock)completionBlock __attribute((deprecated("This method should now be accessed using VMAuthorizationManager")));


/**
 * Disconnect consumer for social account.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param socialTypes - an array of social accounts, currently just "f" for Facebook is supported.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) disconnectSocialAccountWithSocialTypes:(NSArray *)socialTypes
                                         completionBlock:(VMobCompletionBlock)completionBlock __attribute((deprecated("This method should now be accessed using VMAuthorizationManager")));


#pragma mark Favourite Content Methods

/**
 * Marks the Content as specified by the Content Id and Content Type as being a favourite.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param contentId - ID of the content to add as favourite.
 * @param contentType - Type of the content to add as favourite.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *)favouriteContentCreateWithContentId: (NSString*)contentId
                                favouriteContentType: (VMFavourteContentType)contentType
                                     completionBlock: (VMobCompletionBlock)completionBlock;

/**
 * UnMarks the Content as specified by the Content Id and Content Type as not longer being a favourite.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param contentId - ID of the content to remove from favourites.
 * @param contentType - Type of the content to remove from favourites.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *)favouriteContentDeleteWithContentId: (NSString*)contentId
                                favouriteContentType: (VMFavourteContentType)contentType
                                     completionBlock: (VMobCompletionBlock)completionBlock;

/**
 * Get a list of Favourite Content for the consumer.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as VMFavouriteContent* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *)favouriteContentListWithCompletionBlock: (VMobCompletionBlock)completionBlock;


#pragma mark - Feedback support
/**
 * Helps send user feedback to the platform.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param aRating - Rating provided by the user.
 * @param aFeedbackText - Feedback text provided by the consumer.
 * @param aUniqueIdentifier - Feedback group that identifies the type of feedback (e.g. "App feedback", "Restaurant feedback", etc.).
 * @param anExtendedData - Any extra data that needs to be saved with the feedback.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *)feedbackWithRating:(NSNumber *)aRating
                       feedbackText:(NSString *)aFeedbackText
                   uniqueIdentifier:(NSString *)aUniqueIdentifier
                       extendedData:(NSString *)anExtendedData
                    completionBlock:(VMobCompletionBlock)completionBlock;


#pragma mark - get verification token

/**
 * Gets a temporary verification token for the consumer with expiry date for use in POS.
 *
 * @param numericTokenFlag when it is set to true the system will generate a verification token with numeric digits only, other returns an alphanumeric code
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as VMVerificationToken* and the error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *)getVerificationToken:(BOOL)numericTokenFlag
                     completionBlock:(VMobCompletionBlock)completionBlock;
@end

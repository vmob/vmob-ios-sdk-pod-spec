//
//  VMLoyaltyCardAllocatedInstance.h
//  VMobSDK
//
//  Created by VMob on Aug 08, 14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseModel.h"

/**
 * VMLoyaltyCardAllocatedInstance is a class that encapsulates the LoyaltyCard instance allocated information during loyalty point creation with fillMutlipleCards flag.
 */
@interface VMLoyaltyCardAllocatedInstance : VMBaseModel

/**
 * Card's id.
 */
@property (nonatomic, strong) NSNumber *loyaltyCardId;

/**
 * Instance's id.
 */
@property (nonatomic, strong) NSString *loyaltyCardInstanceId;

/**
 * The instance's points balance.
 */
@property (nonatomic, strong) NSNumber *pointsBalance;

/**
 * The instance's points allocated.
 */
@property (nonatomic, strong) NSNumber *pointsAllocated;

/**
 * The instance's outcome code.
 */
@property (nonatomic, strong) NSNumber *outcomeCode;

/**
 * Instance's outcome description.
 */
@property (nonatomic, strong) NSString *outcomeDescription;

@end

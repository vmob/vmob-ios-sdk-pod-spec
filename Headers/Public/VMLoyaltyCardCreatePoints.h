//
//  VMLoyaltyCardCreatePoints
//  VMobSDK
//
//  Created by VMob on 19/03/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseModel.h"

/**
 * VMLoyaltyCardCreatePoints is a class that encapsulates the LoyaltyCard checkin response information.
 */
@interface VMLoyaltyCardCreatePoints : VMBaseModel

/**
 * The id of the loyalty card.
 */
@property (nonatomic, strong) NSNumber *loyaltyCardId;

/**
 * The new points balance.
 */
@property (nonatomic, strong) NSNumber *pointsBalance;

/**
 * The number of points allocated.
 */
@property (nonatomic, strong) NSNumber *pointsAllocated;


/**
 * The list of point allocation info in a fillMultipleCards opted request.
 */
@property (nonatomic, strong) NSArray *pointsCreationSummary;

/**
 * Special error handling added to fill error object if there is case of 'Transaction Id is not valid'.
 */
@property (nonatomic, strong) NSError *error;

@end

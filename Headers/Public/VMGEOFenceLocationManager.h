//
//  VMGEOFenceLocationManager.h
//  geofencedemo
//
//  Created by VMob on 12/09/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "VMBaseLocationManager.h"

/**
 * VMGEOFenceLocationManager is a subclass of {@link #VMCoreLocationManager} providing methods for managing geoFence.
 */
@interface VMGEOFenceLocationManager : VMBaseLocationManager

/**
 * The shared geoFence manager object for the system.
 *
 * @return The systemwide geoFence manager.
 */
+ (instancetype)sharedInstance;

/**
 * Start manually the Geo Location monitoring service.
 * If this service need be to started manually, then first, set autostartGeoFenceService = false
 * in the VMob SDK configuration block in your .plst and call this method at any time.
 */
- (void) startLocationService;

/**
 * Stops all geoFence services.
 */
- (void) stopLocationService;

@end

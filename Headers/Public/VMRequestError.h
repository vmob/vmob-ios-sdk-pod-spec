/*
 * Arello Mobile
 * Mobile Framework
 * Except where otherwise noted, this work is licensed under a Creative Commons Attribution 3.0 Unported License
 * http://creativecommons.org/licenses/by/3.0
 */

#import <Foundation/Foundation.h>


/**
 * Error object which contains error description.
 */

@interface VMRequestError : NSError

/**
 * Construct a request error object.
 *
 * @param code - HTTP status code.
 * @param description - error description.
 * @return a new instance of VMRequestError.
 */
+ (instancetype) requestErrorWithCode:(NSInteger) code description:(NSString *) description;

@end

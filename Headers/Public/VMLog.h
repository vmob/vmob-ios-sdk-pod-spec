//
//  VMLog.h
//  VMobSDK
//
//  Created by VMob on 8/5/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Error log level.
 */
extern NSString *const VMobLogLevelError;
/**
 * Warning log level.
 */
extern NSString *const VMobLogLevelWarning;
/**
 * Info log level.
 */
extern NSString *const VMobLogLevelInfo;
/**
 * Debug log level.
 */
extern NSString *const VMobLogLevelDebug;
/**
 * Vebose log level.
 */
extern NSString *const VMobLogLevelVebose;


/**
 * Custom logger class for VMob SDK.
 */

@interface VMLog : NSObject

/**
 * Initialize logging system is automatically called if logEnabled is true in SDK configuration plist.
 */
+ (void)prepareForLogging;


/**
 * Set log level e.g. VMobLogLevelDebug.
 *
 * @param level - logging level.
 */
+ (void)setLogLevel: (NSString *) level;

/**
 * Error logging
 *
 * @param formatted string and arguments
 */
+ (void)error:(NSString *)format, ... NS_FORMAT_FUNCTION(1,2);


/**
 * Info logging
 *
 * @param formatted string and arguments
 */
+ (void)info:(NSString *)format, ... NS_FORMAT_FUNCTION(1,2);

/**
 * Warning logging
 *
 * @param formatted string and arguments
 */
+ (void)warn:(NSString *)format, ... NS_FORMAT_FUNCTION(1,2);

/**
 * Debug logging
 *
 * @param formatted string and arguments
 */
+ (void)debug:(NSString *)format, ... NS_FORMAT_FUNCTION(1,2);

/**
 * Verbose logging
 *
 * @param formatted string and arguments
 */
+ (void)verbose:(NSString *)format, ... NS_FORMAT_FUNCTION(1,2);

@end

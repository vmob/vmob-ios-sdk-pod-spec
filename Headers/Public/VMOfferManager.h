//
//  VMOfferManager.h
//  VMobSDK
//
//  Created by VMob on 6/1/13.
//  Copyright (c) 2013 VMob. All rights reserved.
//

#import "VMRequestManager.h"
#import "VMConstant.h"
#import "VMOfferSearchCriteria.h"
/**
 * VMOfferManager is a subclass of {@link #VMRequestManager} providing all 'offer' related information available to a consumer.
 */
@interface VMOfferManager : VMRequestManager

/**
 * The shared offer manager object for the system.
 *
 * @return The systemwide offer manager.
 */
+ (instancetype) sharedInstance;

/**
 * Gets a list of merchants who have active offers, optionally filtered by a category id.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param categoryId - ID of a category, used to filter and get only offers from that category.
 * @param offset - Number of merchants that should be skipped from the beginning of the search results.
 * @param limit - Number of merchants that should be returned.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) merchantListWithCategoryId: (NSString *) categoryId
                                      offset: (NSString *) offset
                                       limit: (NSString *) limit
                             completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Gets the full list of main / top-level categories, boolean parameter specifies whether or not to include categories which the current consumer has actively disabled.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param includeConsumerExcludedCategories - Specifies whether or not to include categories which the current consumer has actively disabled.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) categoriesOfferWithIncludeConsumerExcludedCategories: (BOOL) includeConsumerExcludedCategories
                                                       completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Offers are returned in the order of those that have the closest venue come first, the details and distance (in meters) to the closest venue are also returned.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param lat - Latitude used to look for offers nearby.
 * @param lon - Longitude used to look for offers nearby.
 * @param categoryId - ID of a category, used to filter and get only offers from that category. CategoryId is an optional filter that can be supplied, pass an empty or nil value to search all categories.
 * @param offset - Number of offers that should be skipped from the beginning of the search results.
 * @param limit - Number of offers that should be returned.
 * @param fields - Specify exact field names as a comma separated list in the 'fields' parameter to restrict the population of fields with data.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMOffer*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) nearbyOffersWithLatitude: (NSString *) lat
                                 longitude: (NSString *) lon
                                categoryId: (NSString *) categoryId
                                    offset: (NSString *) offset
                                     limit: (NSString *) limit
                                    fields: (NSString *) fields
                           completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Offers are returned by one of 2 formats, either 'Ranked Offers' which have been ranked by a special criteria, e.g. 'Hot Deals' OR queried by category, merchant and/or keyword.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param ranked - Whether offer is ranked.
 * @param keyword - Keyword about offer.
 * @param categoryId - ID of a category, used to filter and get only offers from that category. CategoryId is an optional filter that can be supplied, pass an empty or nil value to search all categories.
 * @param merchantId - ID of merchant.
 * @param fields - Specify exact field names as a comma separated list in the `fields` parameter to restrict the population of fields with data.
 * @param orderBy - OrderBy is case-sensitive and defaults to {@link #kVMOrderByWeighting}, other options are {@link #kVMOrderByTitle}, {@link #kVMOrderByLastUpdatedAt}  and {@link #kVMOrderByStartDate}.
 * @param ascending - Order direction options are `YES` for Ascending and `NO` for Descending.
 * @param ignoreDailyTimeFilter - If 'YES', daypart offers are always returned even if they are not active at the moment of retrieval
 * @param offset - Number of offers that should be skipped from the beginning of the search results.
 * @param limit - Number of offers that should be returned.
 * @param tagsFilterExpression - the expression that will be used to filter the results using tags. E.g. "hot_deals AND expiring_soon OR new_deals".
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMOffer*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *)searchOffersWithRankedSearch:(BOOL)ranked
                                      keyword:(NSString *)keyword
                                   categoryId:(NSString *)categoryId
                                   merchantId:(NSString *)merchantId
                                       fields:(NSString *)fields
                                      orderBy:(NSString *)orderBy
                                    ascending:(BOOL)ascending
                        ignoreDailyTimeFilter:(BOOL)ignoreDailyTimeFilter
                                       offset:(NSNumber *)offset
                                        limit:(NSNumber *)limit
                         tagsFilterExpression:(NSString *)tagsFilterExpression
                              completionBlock:(VMobCompletionBlock)completionBlock __attribute((deprecated("Use method searchOffersWithCriteria instead.")));

/**
 * Offers are returned by one of 2 formats, either 'Ranked Offers' which have been ranked by a special criteria, e.g. 'Hot Deals' OR queried by category, merchant and/or keyword.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param ranked - Whether offer is ranked.
 * @param keyword - Keyword about offer.
 * @param categoryId - ID of a category, used to filter and get only offers from that category. CategoryId is an optional filter that can be supplied, pass an empty or nil value to search all categories.
 * @param merchantId - ID of merchant.
 * @param fields - Specify exact field names as a comma separated list in the `fields` parameter to restrict the population of fields with data.
 * @param orderBy - OrderBy is case-sensitive and defaults to {@link #kVMOrderByWeighting}, other options are {@link #kVMOrderByTitle}, {@link #kVMOrderByLastUpdatedAt}  and {@link #kVMOrderByStartDate}.
 * @param ascending - Order direction options are `YES` for Ascending and `NO` for Descending.
 * @param tagsFilterExpression - the expression that will be used to filter the results using tags. E.g. "hot_deals AND expiring_soon OR new_deals".
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMOffer*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) searchOffersWithRankedSearch: (BOOL) ranked
                                       keyword: (NSString *) keyword
                                    categoryId: (NSString *) categoryId
                                    merchantId: (NSString *) merchantId
                                        fields: (NSString *) fields
                                       orderBy: (NSString *) orderBy
                                     ascending: (BOOL) ascending
                          tagsFilterExpression: (NSString *)tagsFilterExpression
                               completionBlock: (VMobCompletionBlock) completionBlock __attribute((deprecated("Use method searchOffersWithCriteria instead.")));

/**
 * Offers are returned by one of 2 formats, either 'Ranked Offers' which have been ranked by a special criteria, e.g. 'Hot Deals' OR queried by category, merchant and/or keyword.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param ranked - Whether offer is ranked.
 * @param keyword - Keyword about offer.
 * @param categoryId - ID of a category, used to filter and get only offers from that category. CategoryId is an optional filter that can be supplied, pass an empty or nil value to search all categories.
 * @param merchantId - ID of merchant.
 * @param fields - Specify exact field names as a comma separated list in the `fields` parameter to restrict the population of fields with data.
 * @param orderBy - OrderBy is case-sensitive and defaults to {@link #kVMOrderByWeighting}, other options are {@link #kVMOrderByTitle}, {@link #kVMOrderByLastUpdatedAt}  and {@link #kVMOrderByStartDate}.
 * @param ascending - Order direction options are `YES` for Ascending and `NO` for Descending.
 * @param ignoreDailyTimeFilter - If 'YES', daypart offers are always returned even if they are not active at the moment of retrieval.
 * @param tagsFilterExpression - the expression that will be used to filter the results using tags. E.g. "hot_deals AND expiring_soon OR new_deals".
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMOffer*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) searchOffersWithRankedSearch: (BOOL) ranked
                                       keyword: (NSString *) keyword
                                    categoryId: (NSString *) categoryId
                                    merchantId: (NSString *) merchantId
                                        fields: (NSString *) fields
                                       orderBy: (NSString *) orderBy
                                     ascending: (BOOL) ascending
                         ignoreDailyTimeFilter: (BOOL) ignoreDailyTimeFilter
                          tagsFilterExpression: (NSString *)tagsFilterExpression
                               completionBlock: (VMobCompletionBlock) completionBlock __attribute((deprecated("Use method searchOffersWithCriteria instead.")));

/**
 * Offers are returned by one of 2 formats, either 'Ranked Offers' which have been ranked by a special criteria, e.g. 'Hot Deals' OR queried by category, merchant and/or keyword.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param criteria - Offer search criteria {@link VMOfferSearchCriteria}.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMOffer*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */

- (VMOperation *) searchOffersWithCriteria: (VMOfferSearchCriteria *) criteria
                           completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Gets the terms and conditions for a given offer id.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param offerId - ID of the offer to get the terms and conditions for.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and any error that occurred during the request.
 * @return A new cancellable operation.
*/

- (VMOperation *) termsAndConditionsWithOfferId: (NSString *) offerId
                                completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Gets a list of merchants who have active offers, optionally filtered by a category id.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMLoyaltyCard*> and any error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *) loyaltyCardListWithCompletionBlock: (VMobCompletionBlock) completionBlock;


/**
 * Gets a list of merchants who have active offers, optionally filtered by a category id.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param merchantId - ID of merchant.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMVenue*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) merchantVenuesWithMerchantId: (NSNumber *) merchantId
                               completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Get a {@link #VMVenue} information using the ID received, and returns it.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param venueId - ID of venue.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as VMVenue* or NSObject* and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) getVenueWithVenueId: (NSNumber *)venueId
                      completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Get all the {@link #VMVenue}s where the consumer can redeem the offer that matches the ID
 * received, and returns them.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param offerId - ID of offer.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMVenue*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) getVenuesWithOfferId: (NSNumber *)offerId
                       completionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Gets a list of all venues from all merchants in the system.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param offset - Number of venues that should be skipped from the beginning of the search results.
 * @param limit - Number of venues that should be returned.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMVenue*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) getVenuesWithOffset:(NSNumber *)offset
                                limit:(NSNumber *)limit
                      completionBlock:(VMobCompletionBlock)completionBlock;

/**
 * Get a list of external merchants in the system {@link #VMCrossReferenceCMS_1} i.e. 10 excluding hidden merchants.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMExternalMerchant*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *)externalMerchantsWithCompletionBlock:(VMobCompletionBlock) completionBlock;

/**
 * Get a list of external merchants in the system.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param crossReferenceType - System type.
 * @param includeHidden - Whether including hidden external merchants.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMExternalMerchant*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *)externalMerchantsWithSystemType:(VMCrossReferenceType) crossReferenceType
                                   includeHidden:(BOOL) includeHidden
                                 completionBlock:(VMobCompletionBlock) completionBlock;

/**
 * Gets an offer instance given an offerId
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param offerId - ID of the offer to get the terms and conditions for.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as VMOffer* and any error that occurred during the request.
 * @return A new cancellable operation.
 */

- (VMOperation *) getOfferWithId: (NSString *)offerId
                   completionBlock: (VMobCompletionBlock) completionBlock;



#pragma mark - RedeemOffers Methods
/**
 * Takes a snapshot of the current state of the offer and creates a 'redeemed offer' resource.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param offerId - ID of the offer to redeem.
 * @param giftedId - ID of the gifted offer.
 * @param offerInstanceUniqueId - The unique instance id, used for redeeming loyalty offers.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as VMRedeemedOffer* or NSObject* and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) createRedeemedOffersWithOfferId: (NSString *) offerId
                                         giftedID: (NSString *) giftedId
                            offerInstanceUniqueId: (NSString *) offerInstanceUniqueId
                                  completionBlock: (VMobCompletionBlock) completionBlock __attribute((deprecated("Use method redeemOfferWithOfferId instead.")));


/**
 * Takes a snapshot of the current state of the offer and creates a 'redeemed offer' resource.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param offerId - ID of the offer to redeem.
 * @param giftedId - ID of the gifted offer.
 * @param offerInstanceUniqueId - The unique instance id, used for redeeming loyalty offers.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as VMRedeemedOffer* or NSObject* and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) redeemOfferWithOfferId: (NSString *) offerId
                                         giftedID: (NSString *) giftedId
                            offerInstanceUniqueId: (NSString *) offerInstanceUniqueId
                                  completionBlock: (VMobCompletionBlock) completionBlock;



/**
 * Gets a list of offers which have been redeemed by the consumer regardless of the corresponding offer's expiry date.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMRedeemedOffer*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) listRedeemedOffersWithCompletionBlock: (VMobCompletionBlock) completionBlock;

/**
 * Gets a list of offers which have been redeemed by the consumer.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param ignoreStartEndDates - Specify whether or not to get all redeemed offers regardless of of the corresponding offer's expiry date.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMRedeemedOffer*> and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) listRedeemedOffersWithIgnoreStartEndDates:(BOOL) IgnoreStartEndDates completionBlock:(VMobCompletionBlock)completionBlock;

/**
 * Sets a redeemed offer's status to hidden.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param offerId - ID of the offer to delete.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSObject* and any error that occurred during the request.
 * @return A new cancellable operation.
 */
- (VMOperation *) deleteRedeemedOffersWithOfferId: (NSString *) offerId
                         completionBlock: (VMobCompletionBlock) completionBlock;

@end

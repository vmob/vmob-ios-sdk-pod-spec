//
//  CLBeaconRegion+VMBeacon.h
//  VMobSDK
//
//  Created by VMob on 6/08/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "VMBeacon.h"

/**
 * CLBeaconRegion category class to help create {@link #CLBeaconRegion} object from {@link #VMBeacon}.
*/
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
@interface CLBeaconRegion (VMBeacon)

/**
 * Construct new instance of CLBeaconRegion.
 *
 * @param aVMBeacon - {@link #VMBeacon} object.
 * @return new instance of {@link #CLBeaconRegion}.
 */
-(instancetype)initWithVMBeacon:(VMBeacon *)aVMBeacon;

@end
#endif

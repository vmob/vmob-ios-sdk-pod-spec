//
//  VMob.h
//  VMobSDK
//
//  Created by VMob on 30/10/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import <Foundation/Foundation.h>

// constants
#import "VMConstant.h"

// data models
#import "VMAdvertisement.h"
#import "VMBeacon.h"
#import "VMBeaconGroup.h"
#import "VMCategories.h"
#import "VMConsumer.h"
#import "VMExternalMerchant.h"
#import "VMFavouriteContent.h"
#import "VMLoyaltyCard.h"
#import "VMLoyaltyCardAllocatedInstance.h"
#import "VMLoyaltyCardCreatePoints.h"
#import "VMLoyaltyCardInstance.h"
#import "VMLoyaltyCardTransaction.h"
#import "VMMerchant.h"
#import "VMOffer.h"
#import "VMRedeemedOffer.h"
#import "VMVenue.h"
#import "VMVenueOpenHours.h"
#import "VMWeightedContent.h"
#import "VMState.h"
#import "VMCity.h"

// service managers
#import "VMAuthorizationManager.h"
#import "VMActivityManager.h"
#import "VMAdvertisementManager.h"
#import "VMConsumerManager.h"
#import "VMConfigurationManager.h"
#import "VMLocationsManager.h"
#import "VMWeightedContentManager.h"
#import "VMOfferManager.h"
#import "VMGEOFenceLocationManager.h"
#import "VMBeaconManager.h"
#import "VMLoyaltyCardsManager.h"

// useful headers
#import "VMHTTPStatusCodes.h"
#import "VMLog.h"
#import "VMUtils.h"
#import "NSString+VMAdditions.h"
#import "NSDictionary+VMServiceHelper.h"

#define VMobKit [VMob sharedInstance]

/**
 * Convenience class to access any one of the VMob SDK managers to perform related tasks.
 */

@interface VMob : NSObject

+ (instancetype) sharedInstance;


/**
 * VMob SDK initialization.
 * This will be called automatically unless a configuration parameter in VMobSDKConfiguration 
 * named 'autoStartVMobSDK' is overriden with value 'false'.
 *
 */
- (void)initialize;

/**
 * This method returns whether VMobSDK has be intialized or not
 * 
 * @return BOOL true if initialized or else false
 */
- (BOOL)isInitialized;

/**
 * {@link #VMAuthorizationManager} for authorization.
 *
 * @return singleton instance of VMAuthorizationManager.
 */
- (VMAuthorizationManager *)authorizationManager;

/**
 * {@link #VMConfigurationManager} for configurations.
 *
 * @return singleton instance of VMConfigurationManager.
 */
- (VMConfigurationManager *)configurationManager;

/**
 * {@link #VMActivityManager} for tracking activities.
 *
 * @return singleton instance of VMActivityManager.
 */
- (VMActivityManager *)activityManager;

/**
 * {@link #VMConsumerManager} for registration, login and miscellaneous consumer actions.
 *
 * @return singleton instance of VMConsumerManager.
 */
- (VMConsumerManager *)consumerManager;

/**
 * {@link #VMWeightedContentManager} for weighted content.
 *
 * @return singleton instance of VMWeightedContentManager.
 */
- (VMWeightedContentManager *)weightedContentMananger;

/**
 * {@link #VMOfferManager} for offers and redemptions.
 *
 * @return singleton instance of VMOfferManager.
 */
- (VMOfferManager *)offerManager;

/**
 * {@link #VMLoyaltyCardsManager} for loyalty cards.
 *
 * @return singleton instance of VMLoyaltyCardsManager.
 */
- (VMLoyaltyCardsManager *)loyaltyCardsManager;

/**
 * {@link #VMAdvertisementManager} for advertisements.
 *
 * @return singleton instance of VMAdvertisementManager.
 */
- (VMAdvertisementManager *)advertisementManager;

/**
 * {@link #VMGEOFenceLocationManager} for geofences.
 *
 * @return singleton instance of VMGEOFenceLocationManager.
 */
- (VMGEOFenceLocationManager *)geoFenceLocationManager;

/**
 * {@link #VMBeaconManager} for iBeacons
 *
 * @return singleton instance of VMBeaconManager.
 */
- (VMBeaconManager *)beaconManager;

/**
 * {@link #VMLocationsManager} for states and cities.
 *
 * @return singleton instance of VMLocationsManager.
 */
- (VMLocationsManager *)locationsManager;

@end


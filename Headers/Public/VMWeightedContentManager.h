//
//  VMWeightedContentManager.h
//  VMobSDK
//
//  Created by VMob on 13/05/14.
//  Copyright (c) 2014 VMob. All rights reserved.
//

#import "VMRequestManager.h"
#import "VMWeightedContentListCriteria.h"

/**
 * VMWeightedContentManager is a subclass of {@link #VMRequestManager} providing methods for getting weighted content.
 */
@interface VMWeightedContentManager : VMRequestManager

/**
 * The shared manager object for the system.
 *
 * @return The systemwide WeightedContent manager.
 */
+ (instancetype) sharedInstance;

#pragma mark - Weight Content Methods

/**
 * Searches for content based on the search criteria.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param listCriteria - the criteria to be used by the search process.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMWeightedContent*> and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *)weightedContentListWithListCriteria: (VMWeightedContentListCriteria*)listCriteria
                                     completionBlock: (VMobCompletionBlock)completionBlock;
/**
 * Quick searches for content titles matching a given anOrgId.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param anOrgId - The ID of organisation if only needed for a specific organization/merchant (an optional value).
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMWeightedContent*> and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *)weightedContentTitlesWithOrganisationId: (NSString *)anOrgId
                                         completionBlock: (VMobCompletionBlock)completionBlock;

/**
 * The content matching the given id and type.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param anItemID - The ID of the content.
 * @param anItemType - The type of the content.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMWeightedContent*> and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *)weightedContentItemWithId: (NSString *)anItemId
                                   andType: (NSString *)anItemType
                           completionBlock: (VMobCompletionBlock)completionBlock;

/**
 * Gets a list of merchants who have active offers, optionally filtered by a category id.
 *
 * <p/>
 * Errors that can be returned:
 * <ul>
 * 		<li>All the common ones listed on the {@link #VMHTTPStatusCodes} description</li>
 * </ul>
 *
 * @param offset - The number of weighted content items from the beginnig of the list to be returned.
 * @param limit - The maximum number of entries to be returned.
 * @param completionBlock - The block is executed on the completion of a request. This block has no return value and takes two arguments: the object constructed from the response data of the request as NSArray* <VMWeightedContent*> and the error that occurred during the request.
 * @return A new cancellable operation.
*/
- (VMOperation *)weightedContentListWithOffset: (NSString *)  offset
                                         limit: (NSNumber *)  limit
                               completionBlock: (VMobCompletionBlock) completionBlock;

@end

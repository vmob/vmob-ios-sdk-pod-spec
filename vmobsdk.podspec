#
# Be sure to run 'pod lib lint vmobsdk.podspec' to ensure this is a valid podspec
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
Pod::Spec.new do |s|
  s.name             = 'vmobsdk'
  s.version          = '4.28.0'
  s.summary          = 'The vmobsdk distributed as a Pod file'
  s.description      = <<-DESC
  		Please refer to the following page for the release notes of the SDK versions:
  		http://mobile.plexure.com/release_notes/ios.html
			DESC
  s.homepage         = 'http://www.plexure.com'
  s.license          = 'http://mobile.plexure.com/sdk_license.html'
  s.authors          = { 'Kush Sharma' => 'kush.sharma@plexure.com', 'Holmes He' => 'holmes.he@plexure.com' }
  s.source           = { :git => 'git@bitbucket.org:vmob/vmob-ios-sdk-pod-spec.git', :tag => s.version }
  s.source_files     = '**/*.{h,m}'

  s.resources = 'Resources/PlexureInternal.xcdatamodeld'

  s.dependency 'AFNetworking', '3.1.0'
  s.dependency 'CocoaLumberjack', '~> 2.0.0'

  s.platform     = :ios, '7.0'
  s.requires_arc = true
  s.ios.exclude_files = 'Classes/osx'
  s.osx.exclude_files = 'Classes/ios'
  s.public_header_files = 'Headers/Public/**/*.h'
  s.frameworks = 'CoreLocation', 'CoreTelephony', 'SystemConfiguration', 'MobileCoreServices', 'CoreData'

  s.ios.vendored_library = 'Libraries/libVMobSDK.a'

end

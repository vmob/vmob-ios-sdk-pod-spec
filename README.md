## Introduction
This is the distribution repository for the iOS version of the VMob SDK.

## Usage
For integrating Plexure SDK using CocoaPods.

## Requirements
CocoaPods - please refer: https://guides.cocoapods.org/using/using-cocoapods.html for installing/using CocoaPods.

## Installation

* Create a new project.
* Create a podfile and with the following and using the SDK version tag you want to use .e.g.,4.21.0.

```
#!Ruby

pod 'vmobsdk', :git => 'https://bitbucket.org/vmob/vmob-ios-sdk-pod-spec', :tag => '4.21.0'
```

* Close the new project.
* Run `pod install` from the command line.

Then be sure to open the newly created `.xcworkspace` AND NOT the `.xcodeproj`.

## Author

Kush Sharma, kush.sharma@plexure.com

## License
This SDK is provided under a restricted, non-transferrable license under the terms of Plexure's Standard Terms and Conditions as contained in your organization's commercial agreement with Plexure Limited. If your organization does not have a commercial agreement with Plexure, you are not permitted to download, access, or use this SDK in any way. Subject to your compliance with these terms, we grant you a non-exclusive license to install and use the Software (a) in the Territory, (b) consistent with these terms and related documentation accompanying the SDK.
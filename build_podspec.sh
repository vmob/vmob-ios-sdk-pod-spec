#!/bin/bash

PUBLISH_CHANNEL=$1
SDK_VERSION=$2
POD_FILES_DIR=$3

PODSPEC_FILE_NAME="vmobsdk.podspec"
LIB_FILE_NAME="libVMobSDK.a"
XDATAMODEL_FILE_NAME="PlexureInternal.xcdatamodeld"
LIBS_DIR_PATH="Libraries"
HEADERS_DIR_PATH="Headers/Public"
RESOURCES_DIR_PATH="Resources"

printf "Replacing old SDK version with the new in the ${PODSPEC_FILE_NAME} file\n"
# Replace the old SDK version with the new one in the podspec file
OLD_SDK_VERSION=$(cat ${PODSPEC_FILE_NAME} | grep "s.version  " | tr -d " " | cut -f2 -d"=" | tr -d "'")
sed "s/${OLD_SDK_VERSION}/${SDK_VERSION}/g" ${PODSPEC_FILE_NAME} > ${PODSPEC_FILE_NAME}.bak
mv ${PODSPEC_FILE_NAME}.bak ${PODSPEC_FILE_NAME}

printf "\nReplacing old ${LIB_FILE_NAME} file with the new one\n"
# Replace Libraries/libVMobSDK.a with the new .a file
rm -rf ${LIBS_DIR_PATH}/${LIB_FILE_NAME}
cp -p "${POD_FILES_DIR}"/${LIB_FILE_NAME} ${LIBS_DIR_PATH}/

printf "\nReplacing old public header files with the new ones\n"
# Replace public header files with the new ones
rm -rf ${HEADERS_DIR_PATH}/*.h
cp -p "${POD_FILES_DIR}"/include/*.h ${HEADERS_DIR_PATH}/

printf "\nReplacing old xdatamodel file with the new one\n"
# Replace Resources/PlexureInternal.xcdatamodeld with the new ones
rm -rf ${RESOURCES_DIR_PATH}/${XDATAMODEL_FILE_NAME}
cp -r "${POD_FILES_DIR}"/${XDATAMODEL_FILE_NAME} ${RESOURCES_DIR_PATH}/${XDATAMODEL_FILE_NAME}

printf "\nPushing new version to Git...\n"
# We need to set the email and username or the push will fail
git config user.email "build@vmob.com"
git config user.name "BuildBot-v1.1.0"

git checkout ${PUBLISH_CHANNEL}
git add -A .
git commit -m "[VMob-SDK-Builder] Deployed VMob SDK version ${SDK_VERSION}"

git tag -a ${SDK_VERSION} -m "${SDK_VERSION} release"

git push origin ${PUBLISH_CHANNEL}
git push origin ${SDK_VERSION}

printf "\nAll done    (╯°□°）╯︵ ┻━┻\n"
